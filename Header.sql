CREATE OR REPLACE PACKAGE ao_aggregate_package
AS

-- Constants
GC_DELTA        CHAR(1) := 'D';
GC_FULL         CHAR(1) := 'F';

PROCEDURE Identify_Data_Time_Bucket
(
ip_wk_end_dt    ao_pos_data.wk_end_dt%TYPE
);

PROCEDURE Identify_Project_Time_Bucket
(
iv_ProjectKey    ix_ao_project.dbkey%TYPE,
iv_period       ix_ao_project.Desc2%TYPE
);

PROCEDURE replace_zero_cost;

PROCEDURE aggregate_sale_date
(
ip_wk_end_dt    ao_pos_data_sale_date.wk_end_dt%TYPE := NULL
);

PROCEDURE aggregate_sales_store;
PROCEDURE aggregate_sales_store
(
ip_wk_end_dt    ao_pos_data.wk_end_dt%TYPE
);

PROCEDURE aggregate_sales_store_group
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
);

PROCEDURE aggregate_sales_banner
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
);

PROCEDURE aggregate_sales_region
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
);

PROCEDURE aggregate_sales_division
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
);

PROCEDURE ao_aggregate_sales
(
op_out_err         OUT NUMBER,
ip_ProjectKey     ix_ao_project.dbkey%TYPE DEFAULT NULL
);

PROCEDURE ao_post_data_load
(
ip_fullordelta          CHAR := GC_DELTA,
ip_wk_end_dt            ao_pos_data.wk_end_dt%TYPE := NULL
);

PROCEDURE ao_stg_pos_data_load;

PROCEDURE ao_stg_shrink_data_load;

END ao_aggregate_package;

/
