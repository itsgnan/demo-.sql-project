CREATE OR REPLACE PACKAGE BODY GNANI.ao_aggregate_package
AS

-------------------------------------------------------------------------------------------------------------
-- Procedures
-------------------------------------------------------------------------------------------------------------

-- Procedure to identify and populate the list of time buckets for which aggregation
-- is required for the specified project
PROCEDURE Identify_Project_Time_Bucket
(
iv_ProjectKey    ix_ao_project.dbkey%TYPE,
iv_period       ix_ao_project.Desc2%TYPE
)
IS

-- Variables
lv_idx_start    PLS_INTEGER;
lv_idx_next     PLS_INTEGER;
lv_TimeBucket   ao_temp_project.TimeBucket%TYPE;

BEGIN

lv_idx_start := 1;
WHILE TRUE
LOOP
   lv_idx_next := INSTR(iv_period, ',',lv_idx_start);

   IF lv_idx_next <> 0 THEN

      lv_TimeBucket := SUBSTR(iv_period, lv_idx_start, (lv_idx_next-lv_idx_start));
   ELSE
      lv_TimeBucket := SUBSTR(iv_period, lv_idx_start);
   END IF;

   -- Insert into the temp table which holds the periods to calculate for a given project
   INSERT INTO ao_temp_project
   (ProjectID, TimeBucket)
   VALUES
   (iv_ProjectKey, lv_TimeBucket);

   IF lv_idx_next <> 0 THEN
       lv_idx_start := lv_idx_next+1;
   ELSE
       -- Nothing more to do
       EXIT;
   END IF;

END LOOP;

-- Commit the changes
COMMIT WORK;

END Identify_Project_Time_Bucket;

-- Procedure to identify and populate the list of time periods
PROCEDURE Identify_Data_Time_Bucket
(
ip_wk_end_dt    ao_pos_data.wk_end_dt%TYPE
)
IS

-- Types
TYPE ltyp_arr_TimeBucket    IS TABLE OF ao_temp_timebucket.TimeBucket%TYPE
INDEX                       BY PLS_INTEGER;

-- Variables
lv_weeks            PLS_INTEGER;
larr_TimeBucket     ltyp_arr_TimeBucket;

BEGIN

-- Truncate the contents of the table
AO_Purge_Package.AO_Truncate_Table('AO_TEMP_TIMEBUCKET');

-- Initialize
larr_TimeBucket.DELETE;
larr_TimeBucket(1):= '52';
larr_TimeBucket(2):= '26';
larr_TimeBucket(3):= '13';
--larr_TimeBucket(4):= 'YTD';
FOR idx IN 1..larr_TimeBucket.COUNT
LOOP
   IF larr_TimeBucket(idx) = 'YTD' THEN
      lv_weeks := TO_NUMBER(TO_CHAR(ip_wk_end_dt,'IW')) - 1;
   ELSE
      lv_weeks := TO_NUMBER(larr_TimeBucket(idx)) - 1;
   END IF;
   -- Insert rows for the current year
   INSERT INTO ao_temp_timebucket
   (TimeBucket, Year, start_week, end_week)
   VALUES
   (larr_TimeBucket(idx), 'C', (ip_wk_end_dt-(lv_weeks*7)), ip_wk_end_dt);
   -- Insert rows for the last year
   INSERT INTO ao_temp_timebucket
   (TimeBucket, Year, start_week, end_week)
   VALUES
   (larr_TimeBucket(idx), 'L', (ip_wk_end_dt-(52*7)-(lv_weeks*7)), ip_wk_end_dt-(52*7));
END LOOP;

-- Commit the changes
COMMIT WORK;

END Identify_Data_Time_Bucket;

PROCEDURE replace_zero_cost
IS

BEGIN

EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = FALSE';

EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

AO_Purge_Package.AO_Truncate_Table('AO_TEMP_COST');

AO_Purge_Package.AO_Truncate_Table('AO_TEMP_COST_INT');

DBMS_OUTPUT.put_line ('Procedure AO_COST started ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

COMMIT;

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL */
INTO AO_TEMP_COST
(scan_cd,artcl_num,wk_end_dt,str_site_num,cost_amt,cost_amt_inv)
select /*+ NO_STMT_QUEUING FULL(A) PARALLEL(A) */
a.scan_cd,a.artcl_num,a.wk_end_Dt,a.str_site_num,
CASE WHEN a.cost_amt = 0 and a.cost_amt_inv <> 0 then a.cost_amt_inv else a.cost_amt END Cost_amt,
CASE WHEN a.cost_amt_inv = 0 and a.cost_amt <> 0 then a.cost_amt else a.cost_amt_inv END Cost_amt_inv
from ao_pos_data a, ao_temp_timebucket b
WHERE a.wk_end_dt BETWEEN b.start_week AND b.end_week
and b.year = 'C' and b.Timebucket = '52'
and (a.cost_amt = 0 or a.cost_amt_inv = 0)
and a.sale_qty+a.promo_qty <> 0;

DBMS_OUTPUT.put_line ('Table - AO_TEMP_COST was populated ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

COMMIT;

insert /*+USE_HASH PARALLEL(32) */
into AO_TEMP_COST_INT (scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,Type_code)
with pos as
(select /*+ USE_HASH NO_STMT_QUEUING FULL(A,32) PARALLEL(A,32) */
 a.scan_cd,a.artcl_num,a.wk_end_Dt,a.str_site_num,
CASE WHEN a.cost_amt = 0 and a.cost_amt_inv <> 0 then a.cost_amt_inv else a.cost_amt END Cost_amt,
CASE WHEN a.cost_amt_inv = 0 and a.cost_amt <> 0 then a.cost_amt else a.cost_amt_inv END Cost_amt_inv
from ao_pos_data a, ao_temp_timebucket b
 WHERE a.wk_end_dt BETWEEN b.start_week AND b.end_week
 and b.year = 'C' and b.Timebucket = '52' ),
cst_replace as
(
select distinct
scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv
from AO_TEMP_COST
where cost_amt_inv > 0
),
cst_diff as
(
select scan_cd,artcl_num,str_site_num,wk_end_dt from AO_TEMP_COST
minus
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_replace
),
cst_grt as
(
select distinct
pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt,--pos.wk_end_dt
FIRST_VALUE (pos.cost_amt) over (partition by pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt order by pos.wk_end_dt) cost_amt,
FIRST_VALUE (pos.cost_amt_inv) over (partition by pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt order by pos.wk_end_dt) cost_amt_inv
from
pos pos,
cst_diff cst
where pos.scan_cd = cst.scan_cd
and pos.artcl_num = cst.artcl_num
and pos.str_site_num = cst.str_site_num
and pos.wk_end_dt > cst.wk_end_dt
and pos.cost_amt_inv > 0
),
cst_diff_1 as
(
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_diff
minus
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_grt
),
cst_les as
(
select distinct
pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt,--pos.wk_end_dt
FIRST_VALUE (pos.cost_amt) over (partition by pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt order by pos.wk_end_dt desc) cost_amt,
FIRST_VALUE (pos.cost_amt_inv) over (partition by pos.scan_cd,pos.artcl_num,pos.str_site_num,cst.wk_end_dt order by pos.wk_end_dt desc) cost_amt_inv
from
pos pos,
cst_diff_1 cst
where pos.scan_cd = cst.scan_cd
and pos.artcl_num = cst.artcl_num
and pos.str_site_num = cst.str_site_num
and pos.wk_end_dt < cst.wk_end_dt
and pos.cost_amt_inv > 0
),
cst_diff_2 as
(
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_diff_1
minus
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_les
),
cst_bnr as
(
 select distinct
        pos.scan_cd,pos.artcl_num,pos.banner,cst.str_Site_num,cst.wk_end_dt,
        FIRST_VALUE (pos.cost_amt) over (partition by pos.scan_cd,pos.artcl_num,st.desc9 order by pos.wk_end_dt desc,pos.str_site_num desc) cost_amt,
        FIRST_VALUE (pos.cost_amt_inv) over (partition by pos.scan_cd,pos.artcl_num,st.desc9 order by pos.wk_end_dt desc,pos.str_site_num desc) cost_amt_inv
 from
       (select a.scan_cd,a.artcl_num,a.wk_end_Dt,a.str_site_num,st.desc9 banner,Cost_amt,Cost_amt_inv
          from pos a, ix_str_Store st
         where st.storenumber = a.str_Site_num) pos,
       cst_diff_2 cst,
       ix_str_Store  st
 where pos.scan_cd = cst.scan_cd
   and pos.artcl_num = cst.artcl_num
   and pos.banner = st.desc9
   and st.storenumber = cst.str_Site_num
   and pos.cost_amt_inv > 0
),
cst_diff_3 as
(
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_diff_2
minus
select scan_cd,artcl_num,str_site_num,wk_end_dt from cst_bnr
),
cst_any_str as
(
SELECT /*+ PARALLEL(32)*/ distinct a.*,b.str_site_num,b.wk_end_dt
FROM 
(
select /*+ PARALLEL(32)*/
distinct scan_cd,artcl_num,--,str_site_num,wk_end_dt,
FIRST_VALUE (cost_amt) over (partition by scan_cd,artcl_num order by wk_end_dt desc,str_site_num desc) cost_amt,
FIRST_VALUE (cost_amt_inv) over (partition by scan_cd,artcl_num order by wk_end_dt desc,str_site_num desc) cost_amt_inv
FROM
(
select /*+ PARALLEL(32)*/
distinct cst.scan_cd,cst.artcl_num,pos.str_site_num,pos.wk_end_dt,pos.cost_amt,pos.cost_amt_inv
--,--pos.wk_end_dt
--FIRST_VALUE (pos.cost_amt) over (partition by pos.scan_cd,pos.artcl_num order by pos.wk_end_dt desc,pos.str_site_num desc) cost_amt,
--FIRST_VALUE (pos.cost_amt_inv) over (partition by pos.scan_cd,pos.artcl_num order by pos.wk_end_dt desc,pos.str_site_num desc) cost_amt_inv
from
pos pos,
cst_diff_3 cst
where pos.scan_cd = cst.scan_cd
and pos.artcl_num = cst.artcl_num
--and pos.str_site_num = cst.str_site_num
and pos.cost_amt_inv > 0) 
) a ,
cst_diff_3 b
WHERE a.scan_cd = b.scan_cd
and a.artcl_num = b.artcl_num
)
select /*+ PARALLEL(32) */ scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,'0' from cst_replace
union
select /*+ PARALLEL(32) */ scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,'1' from cst_grt
union
select /*+ PARALLEL(32) */ scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,'2' from cst_les
union
select /*+ PARALLEL(32) */ scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,'3' from cst_bnr
union
select /*+ PARALLEL(32) */ scan_cd,artcl_num,str_site_num,wk_end_dt,cost_amt,cost_amt_inv,'4' from cst_any_str;

COMMIT;

DBMS_OUTPUT.put_line ('Table - AO_TEMP_COST_INT was populated ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = TRUE';

EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END replace_zero_cost;

-- Procedure to aggregate sales into multiple time buckets at the store level
-- for all the weeks
PROCEDURE aggregate_sales_store
IS

BEGIN

DBMS_OUTPUT.put_line ('Step Full aggregate_sales_store'  || ' ' || DBMS_UTILITY.get_time );

-- Truncate the existing data
AO_Purge_Package.AO_Truncate_Table('AO_POS_DATA_STORE');

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

INSERT /*+  APPEND PARALLEL(14) */
  INTO ao_pos_data_store
       (
       TimeBucket, str_site_num, scan_cd, artcl_num,sale_uom,
       --ng_code,
       FirstDtSold, sale_qty, sale_amt, tonnage, promo_qty, promo_amt, cost_amt, cost_amt_inv,
       shrink, arcp, ly_sale_qty, ly_sale_amt, ly_tonnage,LastDtSold
       )
SELECT /*+  FULL(A) PARALLEL(A,14) NO_USE_HASH(a b c d)*/
       b.TimeBucket, a.str_site_num, a.scan_cd, a.artcl_num,a.sale_uom,
       --MAX(a.ng_code) ng_code,
       MIN(DECODE(b.year,'C',a.wk_end_dt,NULL)) FirstDtSold,
       SUM(DECODE(b.year,'C',1,0) * a.sale_qty) sale_qty,
       SUM(DECODE(b.year,'C',1,0) * a.sale_amt) sale_amt,
       SUM(DECODE(b.year,'C',1,0) * (a.promo_tonnage+a.tonnage)) tonnage,
       SUM(DECODE(b.year,'C',1,0) * a.promo_qty) promo_qty,
       SUM(DECODE(b.year,'C',1,0) * a.promo_amt) promo_amt,
       SUM(DECODE(b.year,'C',1,0) * (a.promo_qty+a.sale_qty) * DECODE(a.cost_amt,0,d.cost_amt,a.cost_amt)) cost_amt,
       SUM(DECODE(b.year,'C',1,0) * (a.promo_qty+a.sale_qty) * DECODE(a.cost_amt_inv,0,d.cost_amt_inv,a.cost_amt_inv)) cost_amt_inv,
       SUM(DECODE(b.year,'C',1,0) * c.shrink) shrink,
       SUM(DECODE(b.year,'C',1,0) * a.ARCP) arcp,
       SUM(DECODE(b.year,'L',1,0) * (a.promo_qty+a.sale_qty)) ly_sale_qty,
       SUM(DECODE(b.year,'L',1,0) * (a.promo_amt+a.sale_amt)) ly_sale_amt,
       SUM(DECODE(b.year,'L',1,0) * (a.promo_tonnage+a.tonnage)) ly_tonnage,
       MAX(DECODE(b.year,'C',a.wk_end_dt,NULL)) LastDtSold
  FROM (select scan_cd,artcl_num,ng_code,wk_end_dt,str_site_num,sale_qty,sale_amt,promo_tonnage,tonnage,promo_qty, promo_amt, cost_amt, cost_amt_inv,arcp,
               first_value(NVL(sale_uom,0)) over (partition by scan_cd,artcl_num order by wk_end_dt desc) sale_uom
         from ao_pos_data) a
       left join ao_shrink_data c  ON (a.wk_end_dt = c.wk_end_dt
                                   AND a.artcl_num = c.artcl_num
                                   AND a.str_site_num = c.str_site_num)
       left join ao_temp_cost_int d ON (a.wk_end_dt = d.wk_end_dt
                                    AND a.artcl_num = d.artcl_num
                                    AND a.scan_cd = d.scan_cd
                                    AND a.str_site_num = d.str_site_num),
       ao_temp_timebucket b
 WHERE a.wk_end_dt BETWEEN b.start_week AND b.end_week
 GROUP BY a.scan_cd, a.artcl_num,b.TimeBucket, a.str_site_num,a.sale_uom;
--order by a.scan_cd, a.artcl_num,b.TimeBucket, a.str_site_num,a.sale_uom;


-- Commit the changes
COMMIT WORK;

-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';


END aggregate_sales_store;

-- Procedure to aggregate sales at store group level
PROCEDURE aggregate_sales_store_group
(
ip_Projectkey  ix_ao_project.dbkey%TYPE
)
IS

ip_projectid1   PLS_INTEGER;

CURSOR proj_del IS
select DBKey,dbstatus,desc2,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey;


BEGIN

DBMS_OUTPUT.put_line ('Temp_Project Start ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

-- Truncate the existing data
FOR i IN proj_del
LOOP
AO_Purge_Package.AO_Truncate_Table(i.dbkey,'AO_STOREGROUPS_PERFORMANCE');
END LOOP;
AO_Purge_Package.AO_Truncate_Table('AO_TEMP_GRPTOTAL');
AO_Purge_Package.AO_Truncate_Table('AO_TEMP_GRPSOLD');
AO_Purge_Package.AO_Truncate_Table('AO_PRD_POG_STR_REL_TEMP');
AO_Purge_Package.AO_Truncate_Table('AO_POS_DATA_STORE_TEMP');

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = FALSE';

-- Update sale_uom/new_id with latest desc12/desc27 value from ix_spc_product
MERGE /*+ PARALLEL(32) */ INTO ao_product_control pc
USING (SELECT DISTINCT apc.upc,apc.id,
              prd.desc12 sale_uom,
              prd.desc27 new_id,
              apc.dbparentprojectid
         FROM ix_spc_product prd,
              ao_product_control apc
        WHERE apc.upc_cd = prd.upc
          AND apc.id = prd.id
          AND prd.dbstatus = 1) prd 
ON (pc.upc = prd.upc AND
    pc.id = prd.id AND
    pc.dbparentprojectid = prd.dbparentprojectid
   )
WHEN MATCHED THEN UPDATE
SET pc.sale_uom = prd.sale_uom,
    pc.new_id = prd.new_id;

COMMIT;

DBMS_OUTPUT.put_line ('Rolling up the sales of old Article and New Article based on the link between Old and New Article Number ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

INSERT /*+  PARALLEL (32) */ 
INTO AO_POS_DATA_STORE_TEMP
(artcl_num,sale_uom,
 dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups,
 "Category","Group",str_site_num,NUMSTRSG,
 NUMSTRBNR,NUMSTRREG,NUMSTRDIV,
 sale_amt,promo_amt,sale_qty,promo_qty,Tonnage,cost_amt_inv,
 FirstDtSold,Shrink,ARCP,ly_sale_amt,ly_sale_qty,ly_Tonnage,cost_amt,LastDtSold)
WITH pc AS
(SELECT id,upc_cd,upc,
        "Category","Group",new_id,sale_uom,DBParentProjectId
   FROM ao_product_control apc,
        (SELECT DBKey FROM ix_ao_project WHERE (Value3 = 1 OR Value5 = 1 ) AND NVL (ip_Projectkey,DBKey) = DBKey AND dbstatus IN (1,2,3)) c
  WHERE c.DBKey = apc.DBParentProjectID
),
-- New product_control data to bring in missing upc/id based on old id - new id relationship
apc AS
(SELECT * 
   FROM (SELECT upc,id,upc_cd,new_id,sale_uom,DBParentProjectID,"Category","Group",
                -- to pick one when there are multiple primary upc
                ROW_NUMBER() OVER (PARTITION BY id,new_id,sale_uom,DBParentProjectID ORDER BY upc) rnk
           FROM (SELECT pc.upc,pc.id id,upc_cd,
                        pc.new_id,
                        pc.sale_uom,
                        DBParentProjectID,pc."Category",pc."Group"
                   FROM pc
                  UNION
                 SELECT pc.upc,pc.new_id id,upc_cd,
                        NULL new_id,
                        pc.sale_uom,
                        DBParentProjectID,pc."Category",pc."Group"
                   FROM pc
                  WHERE new_id NOT IN (SELECT id FROM pc a WHERE a.DBParentProjectID = pc.DBParentProjectID)
                  UNION
                 SELECT CASE WHEN LENGTH (prd.UPC) IN (11, 12, 13, 14) THEN TO_CHAR(TO_NUMBER(SUBSTR (prd.UPC,1,(LENGTH (prd.UPC) - 1)))) ELSE prd.UPC END upc,
--                        pc.upc,
                        prd.id,upc_cd,
                        prd.desc27 new_id,
                        prd.desc12 sale_uom,
                        DBParentProjectID,pc."Category",pc."Group"
                   FROM (SELECT DISTINCT upc,id,
                                FIRST_VALUE(desc27) OVER (PARTITION BY upc,id ORDER BY dbstatus) desc27,
                                FIRST_VALUE(desc12) OVER (PARTITION BY upc,id ORDER BY dbstatus) desc12
                           FROM ix_spc_product
                          WHERE dbstatus IN (1,4)) prd,
                         pc
                  WHERE prd.desc27 = pc.id
                    AND prd.desc12 = pc.sale_uom))
   WHERE rnk = 1  
),
-- Identify a common upc for the product group (based on old_id - new_id) for aggregation
e AS 
(SELECT apc.upc,apc.id,apc.new_id,apc.sale_uom,apc.DBParentProjectID,
        apc."Category",apc."Group",NVL(a.grp_ident,apc.upc) grp_ident
   FROM apc LEFT OUTER JOIN 
        (SELECT upc,id,new_id,sale_uom,DBParentProjectID,"Category","Group",
                -- Concatenating upc/id/uom to identify unique groups 
                CONNECT_BY_ROOT upc_cd||CONNECT_BY_ROOT id||CONNECT_BY_ROOT sale_uom grp_ident
           FROM apc
           START WITH new_id IS NULL
           CONNECT BY PRIOR id = new_id
                  AND PRIOR sale_uom = sale_uom
                  AND PRIOR DBParentProjectID = DBParentProjectID) a
     ON (apc.upc = a.upc AND 
         apc.id = a.id AND
         apc.DBParentProjectID = a.DBParentProjectID AND
         apc.sale_uom = a.sale_uom)
),
-- Identify the id to which the aggregated value is to be updated
c AS
(SELECT upc_cd,sale_uom,DBParentProjectID,id agg_id,grp_ident
   FROM (SELECT pc.upc,pc.id,pc.upc_cd,pc."Category",pc."Group",pc.new_id,
                pc.sale_uom,pc.DBParentProjectID,e.grp_ident,
                -- to pick one when there are multiple upc for the same new id replacing one old id
                ROW_NUMBER() OVER (PARTITION by e.grp_ident,pc.sale_uom,pc.DBParentProjectID ORDER BY NVL(pc.new_id,0)) cnt
           FROM e,pc
          WHERE e.upc = pc.upc 
            AND e.id = pc.id
            AND e.DBParentProjectID = pc.DBParentProjectID
            AND e.sale_uom = pc.sale_uom)
  WHERE cnt = 1),
b AS
(SELECT e.upc,e.id,e.sale_uom,e.DBParentProjectID,e."Category",e."Group",c.agg_id,c.grp_ident
   FROM e,c
  WHERE e.grp_ident = c.grp_ident
    AND e.sale_uom = c.sale_uom
    AND e.DBParentProjectID = c.DBParentProjectID),
pos AS
( SELECT /*+ PARALLEL(32) */
         DISTINCT b.id,a.agg_id,a.sale_uom,a.grp_ident,
         a.dbparentprojectid,a.timebucket,
         a.Division,a.Region,a.Banner,a.StoreGroups,
         a.str_site_num,rnk,a."Category",a."Group",a.NUMSTRSG,
         a.NUMSTRBNR,a.NUMSTRREG,a.NUMSTRDIV,
         a.sale_amt,a.promo_amt,a.sale_qty,a.promo_qty,a.Tonnage,a.cost_amt_inv,
         a.FirstDtSold,a.Shrink,a.ARCP,a.ly_sale_amt,a.ly_sale_qty,a.ly_Tonnage,a.cost_amt,a.LastDtSold
    FROM b,
         (SELECT /*+ PARALLEL(32) */ 
                scan_cd,artcl_num,agg_id,sale_uom,grp_ident,
                dbparentprojectid,timebucket,
                Division,Region,Banner,StoreGroups,
                str_site_num,
                ROW_NUMBER() OVER (PARTITION BY artcl_num,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups ORDER BY scan_cd) rnk,
                MAX("Category") OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) "Category",
                MAX("Group") OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) "Group",
                MAX(NUMSTRSG) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) NUMSTRSG,
                MAX(NUMSTRBNR) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) NUMSTRBNR,
                MAX(NUMSTRREG) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups)  NUMSTRREG,
                MAX(NUMSTRDIV) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) NUMSTRDIV,
                SUM(sale_amt) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) sale_amt,
                SUM(promo_amt) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) promo_amt,
                SUM(sale_qty) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) sale_qty,
                SUM(promo_qty) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) promo_qty,
                SUM(Tonnage) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) Tonnage,
                SUM(cost_amt_inv) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) cost_amt_inv,
                MIN(FirstDtSold) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) FirstDtSold,
                SUM(Shrink) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) Shrink,
                SUM(ARCP) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) ARCP,
                SUM(ly_sale_amt) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) ly_sale_amt,
                SUM(ly_sale_qty) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) ly_sale_qty,
                SUM(ly_Tonnage) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) ly_Tonnage,
                SUM(cost_amt) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) cost_amt,
                MAX(LastDtSold) OVER (PARTITION BY grp_ident,agg_id,sale_uom,str_site_num,dbparentprojectid,timebucket,Division,Region,Banner,StoreGroups) LastDtSold
          FROM (SELECT /*+ PARALLEL(32)*/
                       scan_cd,artcl_num,
                       agg_id,
                       a.sale_uom,b.grp_ident,
                       b.dbparentprojectid,a.timebucket,
                       d.Division,d.Region,d.Banner,NVL (d.StoreGroups, 'NOSTRGRP') StoreGroups,
                       b."Category",b."Group",d.STORENUMBER str_site_num,d.NUMSTRSG,
                       d.NUMSTRBNR,d.NUMSTRREG,d.NUMSTRDIV,
                       a.sale_amt,a.promo_amt,a.sale_qty,a.promo_qty,a.Tonnage,a.cost_amt_inv,
                       a.FirstDtSold,a.Shrink,a.ARCP,a.ly_sale_amt,a.ly_sale_qty,a.ly_Tonnage,a.cost_amt,a.LastDtSold
                  FROM ao_pos_data_store a,
                       b,
                       ao_assortment_view d,
                       (SELECT DBKey,dbstatus,desc2 FROM ix_ao_project WHERE (Value3 = 1 OR Value5 = 1 ) AND NVL (ip_Projectkey,DBKey) = DBKey AND dbstatus IN (1,2,3)) c
                 WHERE a.artcl_num = b.id
                   AND c.DBKey = b.DBParentProjectID
                   AND c.DBKey = d.DBParentProjectID
                   AND c.desc2 = a.timebucket
                   AND a.str_site_num = d.storenumber
                   AND a.sale_uom = b.sale_uom)
         ) a
   WHERE a.agg_id = b.agg_id
     AND a.grp_ident = b.grp_ident
     AND a.sale_uom = b.sale_uom
     AND a.DBParentProjectID = b.DBParentProjectID)                    
SELECT /*+ PARALLEL(32) */ 
       id artcl_num,sale_uom,
       dbparentprojectid,timebucket,
       Division,Region,Banner,StoreGroups,
       "Category","Group",str_site_num,
       NUMSTRSG,NUMSTRBNR,NUMSTRREG,NUMSTRDIV,
       DECODE(rnk,1,DECODE(id,agg_id,sale_amt,0),0) sale_amt,
       DECODE(rnk,1,DECODE(id,agg_id,promo_amt,0),0) promo_amt,
       DECODE(rnk,1,DECODE(id,agg_id,sale_qty,0),0) sale_qty,
       DECODE(rnk,1,DECODE(id,agg_id,promo_qty,0),0) promo_qty,
       DECODE(rnk,1,DECODE(id,agg_id,Tonnage,0),0) Tonnage,
       DECODE(rnk,1,DECODE(id,agg_id,cost_amt_inv,0),0) cost_amt_inv,
       DECODE(rnk,1,DECODE(id,agg_id,FirstDtSold,NULL),NULL) FirstDtSold,
       DECODE(rnk,1,DECODE(id,agg_id,Shrink,0),0) Shrink,
       DECODE(rnk,1,DECODE(id,agg_id,ARCP,0),0) ARCP,
       DECODE(rnk,1,DECODE(id,agg_id,ly_sale_amt,0),0) ly_sale_amt,
       DECODE(rnk,1,DECODE(id,agg_id,ly_sale_qty,0),0) ly_sale_qty,
       DECODE(rnk,1,DECODE(id,agg_id,ly_Tonnage,0) ,0)ly_Tonnage,
       DECODE(rnk,1,DECODE(id,agg_id,cost_amt,0),0) cost_amt,
       DECODE(rnk,1,DECODE(id,agg_id,LastDtSold,NULL),NULL) LastDtSold
  FROM pos;

COMMIT;

DBMS_OUTPUT.put_line ('Calculate group totals ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL (32) */
INTO AO_TEMP_GRPTOTAL (
   DBPARENTPROJECTID, TIMEBUCKET, DIVISION, 
   REGION, BANNER, STOREGROUPS, 
   "Category", "Group", STORENUMBER, 
   NUMSTRSG, NUMSTRBNR, NUMSTRREG, 
   NUMSTRDIV, GRPTOTALSALESSG, GRPTOTALUNITSSG, 
   GRPTOTALSALESBNR, GRPTOTALUNITSBNR, GRPTOTALSALESRGN, 
   GRPTOTALUNITSRGN, GRPTOTALSALESDIV, GRPTOTALUNITSDIV) 
SELECT /*+ PARALLEL(32) */ 
         DISTINCT
         dbparentprojectid,
         timebucket,
         Division,
         Region,
         Banner,
         StoreGroups,
         "Category",
         "Group",
         str_site_num STORENUMBER,
         NUMSTRSG,
         NUMSTRBNR,
         NUMSTRREG,
         NUMSTRDIV,
         SUM(sale_amt+promo_amt)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            Banner,
                            StoreGroups,
                            "Category",
                            "Group"
            )
            GrpTotalSalesSG,
         SUM(sale_qty+promo_qty)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            Banner,
                            StoreGroups,
                            "Category",
                            "Group"
            )
            GrpTotalUnitsSG,
         SUM(sale_amt+promo_amt)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            Banner,
                            "Category",
                            "Group"
            )
            GrpTotalSalesBNR,
         SUM(sale_qty+promo_qty)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            Banner,
                            "Category",
                            "Group"
            )
            GrpTotalUnitsBNR,
         SUM(sale_amt+promo_amt)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            "Category",
                            "Group"
            )
            GrpTotalSalesRGN,
         SUM(sale_qty+promo_qty)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            Region,
                            "Category",
                            "Group"
            )
            GrpTotalUnitsRGN,
         SUM(sale_amt+promo_amt)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            "Category",
                            "Group"
            )
            GrpTotalSalesDIV,
         SUM(sale_qty+promo_qty)
            OVER (
               PARTITION BY dbparentprojectid,
                            timebucket,
                            Division,
                            "Category",
                            "Group"
            )
            GrpTotalUnitsDIV
  FROM   AO_POS_DATA_STORE_TEMP;

COMMIT;

DBMS_OUTPUT.put_line ('Insert into AO_TEMP_GRPSOLD table ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL(32) */
INTO AO_TEMP_GRPSOLD (
   SCAN_CD, ARTCL_NUM, "Category",
   "Group", 
   --NG_CODE, 
   SALE_AMT,
   SALE_QTY, TONNAGE, PROMO_AMT,
   PROMO_QTY, COST_AMT_INV, FIRSTDTSOLD,
   SHRINK, ARCP, LY_SALE_AMT,
   LY_SALE_QTY, LY_TONNAGE, COST_AMT,
   STR_SITE_NUM, TIMEBUCKET, DBPARENTPROJECTID,
   GRPSALES, GRPUNITS,LASTDTSOLD)
   SELECT   /*+ PARALLEL(32) */ 
            b.upc,
            b.id,
            a."Category",
            a."Group",
            --MAX(a.ng_code) ng_code,
            SUM(a.sale_amt) sale_amt,
            SUM(a.sale_qty) sale_qty,
            SUM(a.Tonnage) Tonnage,
            SUM(a.promo_amt) promo_amt,
            SUM(a.promo_qty) promo_qty,
            SUM(a.cost_amt_inv) cost_amt_inv,
            MIN(a.FirstDtSold) FirstDtSold,
            MAX(a.Shrink) Shrink,
            SUM(a.ARCP) ARCP,
            SUM(a.ly_sale_amt) ly_sale_amt,
            SUM(a.ly_sale_qty) ly_sale_qty,
            SUM(a.ly_Tonnage) ly_Tonnage,
            SUM(a.cost_amt) cost_amt,
            a.str_site_num,
            a.TimeBucket,
            a.DBParentprojectid,
            MIN(a.GrpSales) GrpSales,
            MIN(a.GrpUnits) GrpUnits,
            MAX(a.LastDtSold) LastDtSold
     FROM (SELECT /*+ PARALLEL(32) */ 
                  artcl_num,
                  "Category","Group",
                  --ng_code,
                  sale_amt,sale_qty,Tonnage,promo_amt,promo_qty,cost_amt_inv,
                  FirstDtSold,Shrink,ARCP,ly_sale_amt,ly_sale_qty,ly_Tonnage,cost_amt,
                  str_site_num,TimeBucket,DBParentprojectid,
                  SUM(sale_amt+promo_amt) over (partition by dbparentprojectid, timebucket, str_site_num, "Category", "Group") GrpSales,
                  SUM(sale_qty+promo_qty) over (partition by dbparentprojectid, timebucket, str_site_num, "Category", "Group") GrpUnits,
                  LastDtSold,sale_uom
             FROM AO_POS_DATA_STORE_TEMP) a,
          ao_product_control b
    WHERE a.artcl_num = b.id
      AND a.sale_uom = b.sale_uom
      AND a.DBParentProjectID = b.DBParentProjectID
    GROUP BY b.upc,b.id,a."Category",a."Group",a.str_site_num,a.TimeBucket,a.DBParentprojectid;

COMMIT;

DBMS_OUTPUT.put_line ('Insert into AO_PRD_POG_STR_REL_TEMP table' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

INSERT /*+ PARALLEL(32) */ INTO AO_PRD_POG_STR_REL_TEMP (
       UPC, ID, NG_CODE, 
       DIVISION, REGION, BANNER, 
       STOREGROUPS, DBKEY, MINUNITS, 
       MAXUNITS, MINFACINGS, MAXFACINGS, 
       MINDAYSSUPPLY, MAXDAYSSUPPLY, ORIENTATION_SG, 
       ORIENTATION_BNR, ORIENTATION_RGN, ORIENTATION_DIV, 
       NUMSTRPOGSG, NUMSTRPOGBNR, NUMSTRPOGREG, 
       NUMSTRPOGDIV,AVGFACINGS,AVGDAYSSUPPLY,
       SRQMIN,SRQMAX,SRQAVG,
       MINRETAILFACINGS,MAXRETAILFACINGS,AVGRETAILFACINGS) 
SELECT /*+ PARALLEL(32) */ UPC, ID, NG_CODE, 
       DIVISION, REGION, BANNER, 
       STOREGROUPS, DBKEY, MINUNITS, 
       MAXUNITS, MINFACINGS, MAXFACINGS, 
       MINDAYSSUPPLY, MAXDAYSSUPPLY, ORIENTATION_SG, 
       ORIENTATION_BNR, ORIENTATION_RGN, ORIENTATION_DIV, 
       NUMSTRPOGSG, NUMSTRPOGBNR, NUMSTRPOGREG, 
       NUMSTRPOGDIV,AVGFACINGS,AVGDAYSSUPPLY,
       SRQMIN,SRQMAX,SRQAVG,
       MINRETAILFACINGS,MAXRETAILFACINGS,AVGRETAILFACINGS
  FROM AO_VW_PRD_POG_STR_REL 
 WHERE NVL (ip_Projectkey,DBKey) = DBKey;

COMMIT;

DBMS_OUTPUT.put_line ('SG Start ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL(32) */
  INTO ao_StoreGroups_performance
       (
       UPC, id,
        --ng_code,
       division, region, Banner, StoreGroup,
       DBParentProjectID, TimeBucket,
       TotalSales, TotalUnits, TotalTonnage,
       TotalPromoSales, TotalPromoUnits, PctPromo,
       YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
       PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
       TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
       PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD,
       AVGUPPD,
       AvgPrice, AvgCost, FirstDtSold, NumWkSold, MinUnits, MaxUnits,
       MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
       GrpTotalSales,GrpTotalUnits,GrpSoldSales,GrpSoldUnits, GrpNumWkSold,
       LYTotalSales, LYTotalUnits, LYTotalTonnage,
       TotalCost, TotalARCP
       --s.f
       ,shrink
       ,Orientation,LastDtSold,
       avgfacings,avgdayssupply,
       srqmin,srqmax,srqavg,
       minretailfacings,maxretailfacings,avgretailfacings
       )
 WITH  src AS
       (
       SELECT /*+ NO_STMT_QUEUING FULL(b) NO_STMT_QUEUING FULL(d) PARALLEL(a,32) PARALLEL(d,32)*/
               a.Dbkey, f.TimeBucket, c.Division, c.Region, c.Banner, NVL(c.StoreGroups,'NOSTRGRP') StoreGroups, b."Category", b."Group", b.UPC, b.id,
               --MAX(f.ng_code) ng_code,
               SUM(f.sale_amt) TotalSales,
               SUM(f.sale_qty) TotalUnits,
               SUM(f.Tonnage) TotalTonnage,
               SUM(f.promo_amt) TotalpromoSales,
               SUM(f.promo_qty) TotalpromoUnits,
               SUM((f.sale_amt+f.promo_amt-f.cost_amt_inv)) TotalFEGP,
               SUM(f.Shrink) Shrink,
               SUM(f.ARCP) TotalARCP,
               MAX(c.GrpTotalSalesSG) GrpTotalSales,
               MAX(c.GrpTotalUnitsSG) GrpTotalUnits,
--               SUM(SUM(f.sale_amt)) OVER (PARTITION BY a.ProjectID, f.TimeBucket, c.Division, c.Region, c.Banner, NVL(c.StoreGroups,'NOSTRGRP'), b."Category", b."Group") GrpTotalSales,
--               SUM(SUM(f.sale_qty)) OVER (PARTITION BY a.ProjectID, f.TimeBucket, c.Division, c.Region, c.Banner, NVL(c.StoreGroups,'NOSTRGRP'), b."Category", b."Group") GrpTotalUnits,
               SUM(DECODE(f.FirstDtSold,NULL,0,1) * f.GrpSales) GrpSoldSales,
               SUM(DECODE(f.FirstDtSold,NULL,0,1) * f.GrpUnits) GrpSoldUnits,
               MAX(MAX(((e.end_week-f.FirstDtSold)/7)+1))  OVER (PARTITION BY a.Dbkey, f.TimeBucket, c.Division, c.Region, c.Banner, NVL(c.StoreGroups,'NOSTRGRP'), b."Category", b."Group") GrpNumWkSold,
               SUM(f.ly_sale_amt) LYTotalSales,
               SUM(f.ly_sale_qty) LYTotalUnits,
               SUM(f.ly_Tonnage) LYTotalTonnage,
               SUM(f.cost_amt) Totalcost,
               MIN(f.FirstDtSold) FirstDtSold,
               MAX(((e.end_week-f.FirstDtSold)/7)+1) NumWkSold,
               SUM(DECODE(f.FirstDtSold,NULL,0,1)) NumStrSelling,
               MIN(c.NumStrSG) NumStrTotal,
               MAX(f.LastDtSold) LastDtSold
          -- FROM ao_project_control a, ao_product_control b, ao_assortment_view c, ao_temp_project d, ao_temp_TimeBucket e, ao_pos_data_store f, ao_temp_product g
          -- FROM ao_project_control a, ao_product_control b, ao_assortment_view c, ao_temp_project d, ao_temp_TimeBucket e, ao_pos_data_store f, ao_vw_prd_selldate g
          --FROM ao_project_control a, ao_product_control b, ao_assortment_view c, ao_temp_project d, ao_temp_TimeBucket e, GrpSales f--g , ao_pos_data_store f  ,
          FROM (select DBKey,dbstatus,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey) a,
               ao_product_control b,
               AO_TEMP_GRPTOTAL c,
               ao_temp_project d,
               ao_temp_TimeBucket e,
               AO_TEMP_GRPSOLD f
         WHERE --a.ProjectID = NVL(ip_ProjectID, a.ProjectID)
           --AND
           b.DBParentProjectID = a.Dbkey
           AND c.DBParentProjectID = a.Dbkey
           AND a.dbstatus IN (1,2,3)
           AND d.ProjectID = a.DBKey
           AND e.TimeBucket = d.TimeBucket
           AND e.year = 'C'
           --AND b.upc = '6810089076'
--           AND g.upc = b.upc
--           AND g.ID = b.id
--           AND g.storenumber = c.storenumber
--           AND g.TimeBucket = d.TimeBucket
           AND f.scan_cd = b.UPC
           AND f.artcl_num = b.id
           --s.f AND f.ng_code = b.ng_code
           AND f.str_site_num = c.storenumber
           AND f.TimeBucket = e.TimeBucket
           AND f.DBParentProjectID = a.Dbkey
           AND e.TimeBucket = c.TimeBucket
--           AND f."Category" = c."Category"
--           AND f."Group" = c."Group"
           -- AND g.scan_cd = f.scan_cd
           -- AND g.artcl_num = f.artcl_num
           --s.f AND g.ng_code = f.ng_code
           -- AND g.TimeBucket = f.TimeBucket
           --S.F 10/25/13
           -- AND g.DBParentProjectID = a.ProjectID AND g.Division = C.Division and g.Region = c.Region and g.Banner = c.Banner and g.StoreGroups = NVL(c.StoreGroups,'NOSTRGRP')
--           AND g.upc = f.scan_cd
--           AND g.ID = f.artcl_num
--           AND g.storenumber = f.str_site_num
--           AND g.TimeBucket = f.TimeBucket
         GROUP BY a.DBkey, f.TimeBucket, c.Division, c.Region, c.Banner, NVL(c.StoreGroups,'NOSTRGRP'), b."Category", b."Group", b.UPC, b.id
         --s.f , b.ng_code
         ),
       prd_pog_str_rel AS
       (
       SELECT /*+ PARALLEL(32) */
              DISTINCT UPC, id, division, region, banner, NVL(StoreGroups,'NOSTRGRP') StoreGroups,dbkey,
              MinUnits,
              MaxUnits,
              MinFacings,
              MaxFacings,
              MinDaysSupply,
              MaxDaysSupply,
              Orientation_SG Orientation,
              NumStrPOGSG NumStrPOGed,              
              avgfacings,avgdayssupply,
              srqmin,srqmax,srqavg,
              minretailfacings,maxretailfacings,avgretailfacings
         FROM AO_PRD_POG_STR_REL_TEMP
       ),
       src2 AS
       (
       SELECT a.Dbkey, a.TimeBucket, a.Division, a.Region, a.Banner, a.StoreGroups, a."Category", a."Group", a.UPC, a.id,
              --a.ng_code,
              a.TotalSales, a.TotalUnits, a.TotalTonnage, a.TotalpromoSales, a.TotalpromoUnits, a.TotalFEGP, a.Shrink, a.TotalARCP,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/(a.NumWkSold*a.NumStrSelling))
              END) SPPD,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/(a.NumWkSold*a.NumStrSelling))
              END) UPPD,
              (CASE
                  WHEN a.TotalUnits = 0 THEN 0
                  ELSE (a.TotalSales/a.TotalUnits)
              END) AvgPrice,
              (CASE
                  WHEN (a.TotalUnits+a.TotalpromoUnits) = 0 THEN 0
                  ELSE (a.Totalcost/(a.TotalUnits+a.TotalpromoUnits))
              END) AvgCost,
              (CASE
                  WHEN a.NumWkSold =0 OR a.NumStrTotal = 0 OR a.GrpSoldSales = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/a.NumWkSold)*(a.GrpTotalSales/(a.NumStrTotal*a.GrpSoldSales))
--                  WHEN POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales = 0 THEN 0
--                  ELSE (POWER(a.TotalSales,2)*a.GrpNumWkSold*a.NumStrTotal)/(POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales)
              END) AvgSPPD,
              (CASE
                  WHEN a.NumWkSold =0 OR a.NumStrTotal = 0 OR a.GrpSoldUnits = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/a.NumWkSold)*(a.GrpTotalUnits/(a.NumStrTotal*a.GrpSoldUnits))
--                  WHEN POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales = 0 THEN 0
--                  ELSE (POWER(a.TotalSales,2)*a.GrpNumWkSold*a.NumStrTotal)/(POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales)
              END) AvgUPPD,
              ((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales) yoySaleschange,
              ((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits) yoyUnitschange,
              (a.TotalTonnage-a.LYTotalTonnage) yoyTonnagechange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalpromoSales/(a.TotalSales+a.TotalpromoSales))*100
              END) PctPromo,
              (CASE
                  WHEN a.LYTotalSales = 0 THEN 0
                  ELSE (((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales)/a.LYTotalSales)*100
              END) PctYOYSalesChange,
              (CASE
                  WHEN a.LYTotalUnits = 0 THEN 0
                  ELSE (((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits)/a.LYTotalUnits)*100
              END) PctYOYUnitsChange,
              (CASE
                  WHEN a.LYTotalTonnage = 0 THEN 0
                  ELSE ((a.TotalTonnage-a.LYTotalTonnage)/a.LYTotalTonnage)*100
              END) PctYOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalFEGP/(a.TotalSales+TotalpromoSales))*100
              END) PctMargin,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalARCP/(a.TotalSales+TotalpromoSales))*100
              END) PctARCP,
              (CASE
                  WHEN a.NumStrTotal = 0 THEN 0
                  ELSE (a.NumStrSelling/a.NumStrTotal)*100
              END) PctStrSelling,
              (CASE
                  WHEN a.NumStrTotal = 0 THEN 0
                  ELSE (b.NumStrPOGed/a.NumStrTotal)*100
              END) PctStrPOGed,
              a.NumStrSelling,
              a.FirstDtSold,
              a.NumWkSold,
              b.MinUnits,
              b.MaxUnits,
              b.MinFacings,
              b.MaxFacings,
              b.MinDaysSupply,
              b.MaxDaysSupply,
              b.NumStrPOGed,
              b.Orientation,
              a.GrpTotalSales,a.GrpTotalUnits, a.GrpSoldSales, a.GrpSoldUnits, a.GrpNumWkSold,
              a.LYTotalSales, a.LYTotalUnits, a.LYTotalTonnage,
              a.TotalCost,
              a.LastDtSold,              
              b.avgfacings,
              b.avgdayssupply,
              b.srqmin,
              b.srqmax,
              b.srqavg,
              b.minretailfacings,
              b.maxretailfacings,
              b.avgretailfacings
          FROM src a
          left join prd_pog_str_rel b ON
           (
          b.UPC = a.UPC
          AND b.id = a.id
          AND b.Division = a.Division
          AND b.Region = a.Region
          AND b.Banner = a.Banner
          AND b.StoreGroups = a.StoreGroups
          AND b.dbkey = a.dbkey
           )
         )
       SELECT UPC, id, --ng_code,
              division, region, Banner, StoreGroups,
              DBKey DBParentProjectID, TimeBucket,
              TotalSales, TotalUnits, TotalTonnage,
              TotalPromoSales, TotalPromoUnits, PctPromo,
              YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
              PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
              TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
              PctStrSelling, PctStrPOGed,PctARCP, SPPD, Round(AvgSPPD,2), UPPD,
              Round(AvgUPPD,2),
              AvgPrice, AvgCost, FirstDtSold, Round(NumWkSold,0), MinUnits, MaxUnits,
              MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
              GrpTotalSales, GrpTotalUnits, GrpSoldSales, GrpSoldUnits, GrpNumWkSold,
              LYTotalSales, LYTotalUnits, LYTotalTonnage,
              TotalCost, TotalARCP
              --s.f
               ,shrink
               ,Orientation,LastDtSold,              
              avgfacings,avgdayssupply,srqmin,srqmax,srqavg,
              minretailfacings,maxretailfacings,avgretailfacings
         FROM src2
;

DBMS_OUTPUT.put_line ('SG End ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));

-- Commit the changes
COMMIT WORK;

-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';
EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = TRUE';

END aggregate_sales_store_group;

-- Procedure to aggregate sales at banner level
PROCEDURE aggregate_sales_banner
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
)
IS

CURSOR proj_del IS
select DBKey,dbstatus,desc2,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey;


BEGIN

-- Truncate the existing data
FOR i IN proj_del
LOOP
AO_Purge_Package.AO_Truncate_Table(i.dbkey,'AO_BANNER_PERFORMANCE');
END LOOP;

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL(32) */
  INTO ao_Banner_performance
       (
       UPC, id,
        --ng_code,
       Division, Region, Banner,
       DBParentProjectID, TimeBucket,
       TotalSales, TotalUnits, TotalTonnage,
       TotalPromoSales, TotalPromoUnits, PctPromo,
       YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
       PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
       TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
       PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD,AvgUPPD,
       AvgPrice, AvgCost, FirstDtSold, NumWkSold, MinUnits, MaxUnits,
       MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
       GrpTotalSales,GrpTotalUnits,GrpSoldSales,GrpSoldUnits, GrpNumWkSold,
       LYTotalSales, LYTotalUnits, LYTotalTonnage,
       TotalCost, TotalARCP
       --s.f
       ,shrink
       ,Orientation,LastDtSold,
       avgfacings,avgdayssupply,
       srqmin,srqmax,srqavg,
       minretailfacings,maxretailfacings,avgretailfacings
       )
  WITH src AS
       (
       SELECT /*+ NO_STMT_QUEUING FULL(A) PARALLEL(A,32) */
               a.DBKey, b.TimeBucket, b.Division, b.Region, b.Banner, b.UPC, b.ID,
               --MAX(b.ng_code) ng_code,
               SUM(b.TotalSales) TotalSales,
               SUM(b.TotalUnits) TotalUnits,
               SUM(b.TotalTonnage) TotalTonnage,
               SUM(b.TotalPromoSales) TotalPromoSales,
               SUM(b.TotalPromoUnits) TotalPromoUnits,
               SUM(b.TotalFEGP) TotalFEGP,
               SUM(b.Shrink) Shrink,
               SUM(b.TotalARCP) TotalARCP,
               MAX(b.NumWkSold) NumWkSold,
               SUM(b.NumStrSelling) NumStrSelling,
               MIN(b.FirstDtSold) FirstDtSold,
--               SUM(b.GrpTotalSales) GrpTotalSales,
--               SUM(b.GrpTotalUnits) GrpTotalUnits,
               SUM(b.GrpSoldSales) GrpSoldSales,
               SUM(b.GrpSoldUnits) GrpSoldUnits,
               MAX(b.GrpNumWkSold) GrpNumWkSold,
               SUM(b.LYTotalUnits) LYTotalUnits,
               SUM(b.LYTotalSales) LYTotalSales,
               SUM(b.LYTotalTonnage) LYTotalTonnage,
               SUM(b.TotalCost) TotalCost,
               MAX(b.LastDtSold) LastDtSold
          FROM (select DBKey,dbstatus,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey) a, ao_storegroups_performance b
         WHERE --a.ProjectID = NVL(ip_ProjectID, a.ProjectID)
           --AND
           b.DBParentProjectID = a.DBKey
           AND a.dbstatus IN (1,2,3)
         GROUP BY a.DBKey, b.TimeBucket, b.Division, b.Region, b.Banner, b.UPC, b.ID
         --s.f, b.ng_code
       ),
       prd_pog_str_rel AS
       (
       SELECT /*+ PARALLEL(32) */
              UPC, id,
       --s.fng_code,
       Division, Region, Banner,dbkey,
              MIN(MinUnits) MinUnits,
              MAX(MaxUnits) MaxUnits,
              MIN(Minfacings) MinFacings,
              MAX(Maxfacings) MaxFacings,
              Min(Orientation_BNR) Orientation,
              MIN(Mindayssupply) MinDaysSupply,
              MAX(Maxdayssupply) MaxDaysSupply,
              MAX(NumStrPOGBnr) NumStrPOGed,                            
              AVG(avgfacings) avgfacings,
              AVG(avgdayssupply) avgdayssupply,
              MIN(srqmin) srqmin,
              MAX(srqmax) srqmax,
              AVG(srqavg) srqavg,
              MIN(minretailfacings) minretailfacings,
              MAX(maxretailfacings) maxretailfacings,
              AVG(avgretailfacings) avgretailfacings
         FROM AO_PRD_POG_STR_REL_TEMP
        GROUP BY UPC, id,
        --s.f ng_code,
        Division, Region, Banner, dbkey
       ),
       assortment AS
       (
       SELECT DBParentProjectID, Division, Region, Banner,"Category", "Group", Timebucket,
              MAX(NumStrBnr) NumStrTotal,
              MAX(GrpTotalSalesBNR) GrpTotalSales,
              MAX(GrpTotalUnitsBNR) GrpTotalUnits
         --FROM ao_assortment_view
         FROM AO_TEMP_GRPTOTAL
         Where DBParentProjectID = NVL(ip_ProjectKey, DBParentProjectID)
        GROUP BY DBParentProjectID, Division, Region, Banner,"Category", "Group", Timebucket
       ),
       src2 AS
       (
       SELECT a.DBKey, a.TimeBucket, a.Division, a.Region, a.Banner, a.UPC, a.id,
              --a.ng_code,
              a.TotalSales, a.TotalUnits, a.TotalTonnage, a.TotalpromoSales, a.TotalpromoUnits, a.TotalFEGP, a.Shrink, a.TotalARCP,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/(a.NumWkSold*NumStrSelling))
              END) SPPD,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/(a.NumWkSold*NumStrSelling))
              END) UPPD,
              (CASE
                  WHEN a.TotalUnits = 0 THEN 0
                  ELSE (a.TotalSales/a.TotalUnits)
              END) AvgPrice,
              (CASE
                  WHEN (a.TotalUnits+a.TotalpromoUnits) = 0 THEN 0
                  ELSE (a.Totalcost/(a.TotalUnits+a.TotalpromoUnits))
              END) AvgCost,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldSales = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/a.NumWkSold)*(c.GrpTotalSales/(c.NumStrTotal*a.GrpSoldSales))
                  --WHEN POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales = 0 THEN 0
                  --ELSE (POWER(a.TotalSales,2)*a.GrpNumWkSold*a.NumStrTotal)/(POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales)
              END) AvgSPPD,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldUnits = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/a.NumWkSold)*(c.GrpTotalUnits/(c.NumStrTotal*a.GrpSoldUnits))
              END) AvgUPPD,
              ((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales) YOYSalesChange,
              ((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits) YOYUnitsChange,
              (a.TotalTonnage-a.LYTotalTonnage) YOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalpromoSales/(a.TotalSales+a.TotalpromoSales))*100
              END) PctPromo,
              (CASE
                  WHEN a.LYTotalSales = 0 THEN 0
                  ELSE (((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales)/a.LYTotalSales)*100
              END) PctYOYSalesChange,
              (CASE
                  WHEN a.LYTotalUnits = 0 THEN 0
                  ELSE (((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits)/a.LYTotalUnits)*100
              END) PctYOYUnitsChange,
              (CASE
                  WHEN a.LYTotalTonnage = 0 THEN 0
                  ELSE ((a.TotalTonnage-a.LYTotalTonnage)/a.LYTotalTonnage)*100
              END) PctYOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalFEGP/(a.TotalSales+TotalpromoSales))*100
              END) PctMargin,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalARCP/(a.TotalSales+TotalpromoSales))*100
              END) PctARCP,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (a.NumStrSelling/c.NumStrTotal)*100
              END) PctStrSelling,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (b.NumStrPOGed/c.NumStrTotal)*100
              END) PctStrPOGed,
              a.NumStrSelling,
              a.FirstDtSold,
              a.NumWkSold,
              b.MinUnits,
              b.MaxUnits,
              b.MinFacings,
              b.MaxFacings,
              b.MinDaysSupply,
              b.MaxDaysSupply,
              b.Orientation,
              b.NumStrPOGed,
              c.GrpTotalSales,c.GrpTotalUnits, a.GrpSoldSales, a.GrpSoldUnits,
              a.GrpNumWkSold,
              a.LYTotalUnits,
              a.LYTotalSales,
              a.LYTotalTonnage,
              a.TotalCost,
              a.LastDtSold,              
              b.avgfacings,
              b.avgdayssupply,
              b.srqmin,
              b.srqmax,
              b.srqavg,
              b.minretailfacings,
              b.maxretailfacings,
              b.avgretailfacings
         FROM src a
             left join prd_pog_str_rel b ON
               (
                  b.UPC = a.UPC
                  AND b.id = a.id
                  --s.f AND b.ng_code = a.ng_code
                  AND b.Division = a.Division
                  AND b.Region = a.Region
                  AND b.Banner = a.Banner
                  AND b.dbkey = a.dbkey
               )
            , assortment c
        WHERE
              c.DBParentProjectID = a.DBKey
          AND c.Division = a.Division
          AND c.Region = a.Region
          AND c.Banner = a.Banner
          AND c.Timebucket = a.Timebucket
--          AND c."Category" = a."Category"
--          AND c."Group" = a."Group"
       )
       SELECT UPC,ID, --ng_code,
       Division, Region, Banner,
              DBKey DBParentProjectID, TimeBucket,
              TotalSales, TotalUnits, TotalTonnage,
              TotalPromoSales, TotalPromoUnits, PctPromo,
              YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
              PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
              TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
              PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD, AvgUPPD,
              AvgPrice, AvgCost, FirstDtSold, Round(NumWkSold,0) NumWkSold, MinUnits, MaxUnits,
              MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
              GrpTotalSales, GrpTotalUnits,GrpSoldSales,GrpSoldUnits,GrpNumWkSold,
              LYTotalSales, LYTotalUnits, LYTotalTonnage,
              TotalCost, TotalARCP
             --s.f
             ,shrink
             ,Orientation,LastDtSold,              
              avgfacings,avgdayssupply,srqmin,srqmax,srqavg,
              minretailfacings,maxretailfacings,avgretailfacings
         FROM src2
;

-- Commit the changes
COMMIT WORK;
-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END aggregate_sales_banner;

-- Procedure to aggregate sales at region level
PROCEDURE aggregate_sales_region
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
)
IS

CURSOR proj_del IS
select DBKey,dbstatus,desc2,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey;


BEGIN

-- Truncate the existing data

FOR i IN proj_del
LOOP
AO_Purge_Package.AO_Truncate_Table(i.dbkey,'AO_REGION_PERFORMANCE');
END LOOP;

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL(32) */
  INTO ao_region_performance
       (
       UPC, id,
       --ng_code,
       Division, Region,
       DBParentProjectID, TimeBucket,
       TotalSales, TotalUnits, TotalTonnage,
       TotalPromoSales, TotalPromoUnits, PctPromo,
       YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
       PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
       TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
       PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD,AvgUPPD,
       AvgPrice, AvgCost, FirstDtSold, NumWkSold, MinUnits, MaxUnits,
       MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
       GrpTotalSales,GrpTotalUnits,GrpSoldSales,GrpSoldUnits, GrpNumWkSold,
       LYTotalSales, LYTotalUnits, LYTotalTonnage,
       TotalCost, TotalARCP
       --s.f
       ,shrink
       ,Orientation,LastDtSold,
       avgfacings,avgdayssupply,
       srqmin,srqmax,srqavg,
       minretailfacings,maxretailfacings,avgretailfacings
       )
  WITH src AS
       (
       SELECT /*+ NO_STMT_QUEUING FULL(A) PARALLEL(A,32) */
               a.DBKey, b.TimeBucket, b.Division, b.Region, b.UPC, b.ID,
               --MAX(b.ng_code) ng_code,
               SUM(b.TotalSales) TotalSales,
               SUM(b.TotalUnits) TotalUnits,
               SUM(b.TotalTonnage) TotalTonnage,
               SUM(b.TotalpromoSales) TotalpromoSales,
               SUM(b.TotalpromoUnits) TotalpromoUnits,
               SUM(b.TotalFEGP) TotalFEGP,
               SUM(b.Shrink) Shrink,
               SUM(b.TotalARCP) TotalARCP,
               MAX(b.NumWkSold) NumWkSold,
               SUM(b.NumStrSelling) NumStrSelling,
               MIN(b.FirstDtSold) FirstDtSold,
--               SUM(b.GrpTotalSales) GrpTotalSales,
--               SUM(b.GrpTotalUnits) GrpTotalUnits,
               SUM(b.GrpSoldSales) GrpSoldSales,
               SUM(b.GrpSoldUnits) GrpSoldUnits,
               MAX(b.GrpNumWkSold) GrpNumWkSold,
               SUM(b.LYTotalUnits) LYTotalUnits,
               SUM(b.LYTotalSales) LYTotalSales,
               SUM(b.LYTotalTonnage) LYTotalTonnage,
               SUM(b.TotalCost) TotalCost,
               MAX(b.LastDtSold) LastDtSold
          FROM (select DBKey,dbstatus,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey) a, ao_banner_performance b
         WHERE --a.ProjectID = NVL(ip_ProjectID, a.ProjectID)
           --AND
           b.DBParentProjectID = a.DBKey
           AND a.dbstatus IN (1,2,3)
         GROUP BY a.DBKey, b.TimeBucket, b.Division, b.Region, b.UPC, b.ID
         --s.f, b.ng_code
       ),
       prd_pog_str_rel AS
       (
       SELECT /*+ PARALLEL(32) */
              UPC, id,
       --s.fng_code,
       Division, Region, dbkey,
              MIN(MinUnits) MinUnits,
              MAX(MaxUnits) MaxUnits,
              MIN(Minfacings) MinFacings,
              MAX(Maxfacings) MaxFacings,
              Min(Orientation_RGN) Orientation,
              MIN(Mindayssupply) MinDaysSupply,
              MAX(Maxdayssupply) MaxDaysSupply,
              MAX(NumStrPOGReg) NumStrPOGed,                            
              AVG(avgfacings) avgfacings,
              AVG(avgdayssupply) avgdayssupply,
              MIN(srqmin) srqmin,
              MAX(srqmax) srqmax,
              AVG(srqavg) srqavg,
              MIN(minretailfacings) minretailfacings,
              MAX(maxretailfacings) maxretailfacings,
              AVG(avgretailfacings) avgretailfacings
         FROM AO_PRD_POG_STR_REL_TEMP
        GROUP BY UPC, id,
        --s.fng_code,
        Division, Region, dbkey
       ),
       assortment AS
       (
       SELECT DBParentProjectID, Division, Region, "Category", "Group", Timebucket,
              MAX(NumStrReg) NumStrTotal,
              MAX(GRPTOTALSALESRGN) GrpTotalSales,
              MAX(GRPTOTALUNITSRGN) GrpTotalUnits
         --FROM ao_assortment_view
         FROM AO_TEMP_GRPTOTAL
         Where DBParentProjectID = NVL(ip_ProjectKey, DBParentProjectID)
        GROUP BY DBParentProjectID, Division, Region, "Category", "Group", Timebucket
       ),
       src2 AS
       (
       SELECT a.DBKey, a.TimeBucket, a.Division, a.Region, a.UPC, a.id,
              --a.ng_code,
              a.TotalSales, a.TotalUnits, a.TotalTonnage, a.TotalpromoSales, a.TotalpromoUnits, a.TotalFEGP, a.Shrink, a.TotalARCP,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/(a.NumWkSold*NumStrSelling))
              END) SPPD,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/(a.NumWkSold*NumStrSelling))
              END) UPPD,
              (CASE
                  WHEN a.TotalUnits = 0 THEN 0
                  ELSE (a.TotalSales/a.TotalUnits)
              END) AvgPrice,
              (CASE
                  WHEN (a.TotalUnits+a.TotalpromoUnits) = 0 THEN 0
                  ELSE (a.Totalcost/(a.TotalUnits+a.TotalpromoUnits))
              END) AvgCost,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldSales = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/a.NumWkSold)*(c.GrpTotalSales/(c.NumStrTotal*a.GrpSoldSales))
                  --WHEN POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales = 0 THEN 0
                  --ELSE (POWER(a.TotalSales,2)*a.GrpNumWkSold*a.NumStrTotal)/(POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales)
              END) AvgSPPD,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldUnits = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoSales)/a.NumWkSold)*(c.GrpTotalUnits/(c.NumStrTotal*a.GrpSoldUnits))
              END) AvgUPPD,
              ((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales) YOYSalesChange,
              ((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits) YOYUnitsChange,
              (a.TotalTonnage-a.LYTotalTonnage) YOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalpromoSales/(a.TotalSales+a.TotalpromoSales))*100
              END) PctPromo,
              (CASE
                  WHEN a.LYTotalSales = 0 THEN 0
                  ELSE (((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales)/a.LYTotalSales)*100
              END) PctYOYSalesChange,
              (CASE
                  WHEN a.LYTotalUnits = 0 THEN 0
                  ELSE (((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits)/a.LYTotalUnits)*100
              END) PctYOYUnitsChange,
              (CASE
                  WHEN a.LYTotalTonnage = 0 THEN 0
                  ELSE ((a.TotalTonnage-a.LYTotalTonnage)/a.LYTotalTonnage)*100
              END) PctYOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalFEGP/(a.TotalSales+TotalpromoSales))*100
              END) PctMargin,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalARCP/(a.TotalSales+TotalpromoSales))*100
              END) PctARCP,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (a.NumStrSelling/c.NumStrTotal)*100
              END) PctStrSelling,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (b.NumStrPOGed/c.NumStrTotal)*100
              END) PctStrPOGed,
              a.NumStrSelling,
              a.FirstDtSold,
              a.NumWkSold,
              b.MinUnits,
              b.MaxUnits,
              b.MinFacings,
              b.MaxFacings,
              b.MinDaysSupply,
              b.MaxDaysSupply,
              b.Orientation,
              b.NumStrPOGed,
              c.GrpTotalSales,c.GrpTotalUnits, a.GrpSoldSales, a.GrpSoldUnits,
              a.GrpNumWkSold,
              a.LYTotalUnits,
              a.LYTotalSales,
              a.LYTotalTonnage,
              a.TotalCost,
              a.LastDtSold,              
              b.avgfacings,
              b.avgdayssupply,
              b.srqmin,
              b.srqmax,
              b.srqavg,
              b.minretailfacings,
              b.maxretailfacings,
              b.avgretailfacings
         FROM src a
             left join prd_pog_str_rel b ON
               (
                  b.UPC = a.UPC
                  AND b.id = a.id
                  --s.f AND b.ng_code = a.ng_code
                  AND b.Division = a.Division
                  AND b.Region = a.Region
                  AND b.dbkey = a.dbkey
               )
         , assortment c
        WHERE
          c.DBParentProjectID = a.DBKey
          AND c.Division = a.Division
          AND c.Region = a.Region
          AND c.Timebucket = a.Timebucket
--          AND c."Category" = a."Category"
--          AND c."Group" = a."Group"
       )
       SELECT UPC,ID, --ng_code,
       Division, Region,
              DBKey DBParentProjectID, TimeBucket,
              TotalSales, TotalUnits, TotalTonnage,
              TotalPromoSales, TotalPromoUnits, PctPromo,
              YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
              PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
              TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
              PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD, AvgUPPD,
              AvgPrice, AvgCost, FirstDtSold, Round(NumWkSold,0) NumWkSold, MinUnits, MaxUnits,
              MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply,
              GrpTotalSales,GrpTotalUnits,GrpSoldSales,GrpSoldUnits, GrpNumWkSold,
              LYTotalSales, LYTotalUnits, LYTotalTonnage,
              TotalCost, TotalARCP
             --s.f
             ,shrink
             ,Orientation,LastDtSold,              
              avgfacings,avgdayssupply,srqmin,srqmax,srqavg,
              minretailfacings,maxretailfacings,avgretailfacings
         FROM src2
;

-- Commit the changes
COMMIT WORK;

-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END aggregate_sales_region;

-- Procedure to aggregate sales at division level
PROCEDURE aggregate_sales_division
(
ip_ProjectKey  ix_ao_project.dbkey%TYPE
)
IS

CURSOR proj_del IS
select DBKey,dbstatus,desc2,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey;

BEGIN

-- Truncate the existing data
FOR i IN proj_del
LOOP
AO_Purge_Package.AO_Truncate_Table(i.dbkey,'AO_DIVISION_PERFORMANCE');
END LOOP;

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

INSERT /*+ NO_STMT_QUEUING APPEND PARALLEL(32) */
  INTO ao_division_performance
       (
       UPC, ID,
       --ng_code,
       Division,
       DBParentProjectID, TimeBucket,
       TotalSales, TotalUnits, TotalTonnage,
       TotalPromoSales, TotalPromoUnits, PctPromo,
       YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
       PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
       TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
       PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD,AvgUPPD,
       AvgPrice, AvgCost, FirstDtSold, NumWkSold, MinUnits, MaxUnits,
       MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply
       --s.f
       ,shrink
       ,Orientation,LastDtSold,
       avgfacings,avgdayssupply,
       srqmin,srqmax,srqavg,
       minretailfacings,maxretailfacings,avgretailfacings
       )
  WITH src AS
       (
       SELECT /*+ NO_STMT_QUEUING FULL(A) PARALLEL(A,32) */
               a.DBKey, b.TimeBucket, b.Division, b.UPC, b.ID,
               --MAX(b.ng_code) ng_code,
               SUM(b.TotalSales) TotalSales,
               SUM(b.TotalUnits) TotalUnits,
               SUM(b.TotalTonnage) TotalTonnage,
               SUM(b.TotalpromoSales) TotalpromoSales,
               SUM(b.TotalpromoUnits) TotalpromoUnits,
               SUM(b.TotalFEGP) TotalFEGP,
               SUM(b.Shrink) Shrink,
               SUM(b.TotalARCP) TotalARCP,
               MAX(b.NumWkSold) NumWkSold,
               SUM(b.NumStrSelling) NumStrSelling,
               MIN(b.FirstDtSold) FirstDtSold,
--               SUM(b.GrpTotalSales) GrpTotalSales,
--               SUM(b.GrpTotalUnits) GrpTotalUnits,
               SUM(b.GrpSoldSales) GrpSoldSales,
               SUM(b.GrpSoldUnits) GrpSoldUnits,
               MAX(b.GrpNumWkSold) GrpNumWkSold,
               SUM(b.LYTotalUnits) LYTotalUnits,
               SUM(b.LYTotalSales) LYTotalSales,
               SUM(b.LYTotalTonnage) LYTotalTonnage,
               SUM(b.TotalCost) TotalCost,
               MAX(b.LastDtSold) LastDtSold
          FROM (select DBKey,dbstatus,rownum from ix_ao_project where (Value3 = 1 OR Value5 = 1 ) and NVL (ip_Projectkey,DBKey) = DBKey) a, ao_region_performance b
         WHERE --a.ProjectID = NVL(ip_ProjectID, a.ProjectID)
           --AND
           b.DBParentProjectID = a.DBKey
           AND a.dbstatus IN (1,2,3)
         GROUP BY a.DBKey, b.TimeBucket, b.Division, b.UPC, b.ID
         --s.f, b.ng_code
       ),
       prd_pog_str_rel AS
       (
       SELECT /*+ PARALLEL(32) */
              UPC, ID, Division,dbkey,
              MIN(MinUnits) MinUnits,
              MAX(MaxUnits) MaxUnits,
              MIN(Minfacings) MinFacings,
              MAX(Maxfacings) MaxFacings,
              MIN(Mindayssupply) MinDaysSupply,
              MAX(Maxdayssupply) MaxDaysSupply,
              MIN(Orientation_DIV) Orientation,
              MAX(NumStrPOGDiv) NumStrPOGed,                            
              AVG(avgfacings) avgfacings,
              AVG(avgdayssupply) avgdayssupply,
              MIN(srqmin) srqmin,
              MAX(srqmax) srqmax,
              AVG(srqavg) srqavg,
              MIN(minretailfacings) minretailfacings,
              MAX(maxretailfacings) maxretailfacings,
              AVG(avgretailfacings) avgretailfacings
         FROM AO_PRD_POG_STR_REL_TEMP
        GROUP BY UPC, ID, Division,dbkey
       ),
       assortment AS
       (
       SELECT DBParentProjectID, Division, "Category", "Group", TimeBucket,
              MAX(NumStrDiv) NumStrTotal,
              MAX(GRPTOTALSALESDIV) GrpTotalSales,
              MAX(GRPTOTALUNITSDIV) GrpTotalUnits
         --FROM ao_assortment_view
         FROM AO_TEMP_GRPTOTAL
         Where DBParentProjectID = NVL(ip_ProjectKey, DBParentProjectID)
        GROUP BY DBParentProjectID, Division, "Category", "Group", Timebucket
       ),
       src2 AS
       (
       SELECT a.DBKey, a.TimeBucket, a.Division, a.UPC, a.id,
              --a.ng_code,
              a.TotalSales, a.TotalUnits, a.TotalTonnage, a.TotalpromoSales, a.TotalpromoUnits, a.TotalFEGP, a.Shrink, a.TotalARCP,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/(a.NumWkSold*NumStrSelling))
              END) SPPD,
              (CASE
                  WHEN (a.NumWkSold*NumStrSelling) = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/(a.NumWkSold*NumStrSelling))
              END) UPPD,
              (CASE
                  WHEN a.TotalUnits = 0 THEN 0
                  ELSE (a.TotalSales/a.TotalUnits)
              END) AvgPrice,
              (CASE
                  WHEN (a.TotalUnits+a.TotalpromoUnits) = 0 THEN 0
                  ELSE (a.Totalcost/(a.TotalUnits+a.TotalpromoUnits))
              END) AvgCost,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldSales = 0 THEN 0
                  ELSE ((a.TotalSales+a.TotalpromoSales)/a.NumWkSold)*(c.GrpTotalSales/(c.NumStrTotal*a.GrpSoldSales))
                  --WHEN POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales = 0 THEN 0
                  --ELSE (POWER(a.TotalSales,2)*a.GrpNumWkSold*a.NumStrTotal)/(POWER(a.NumWkSold*NumStrSelling,2)*GrpTotalSales)
              END) AvgSPPD,
              (CASE
                  WHEN a.NumWkSold =0 OR c.NumStrTotal = 0 OR a.GrpSoldUnits = 0 THEN 0
                  ELSE ((a.TotalUnits+a.TotalpromoUnits)/a.NumWkSold)*(c.GrpTotalUnits/(c.NumStrTotal*a.GrpSoldUnits))
              END) AvgUPPD,
              ((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales) YOYSalesChange,
              ((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits) YOYUnitsChange,
              (a.TotalTonnage-a.LYTotalTonnage) YOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalpromoSales/(a.TotalSales+a.TotalpromoSales))*100
              END) PctPromo,
              (CASE
                  WHEN a.LYTotalSales = 0 THEN 0
                  ELSE (((a.TotalSales+a.TotalpromoSales)-a.LYTotalSales)/a.LYTotalSales)*100
              END) PctYOYSalesChange,
              (CASE
                  WHEN a.LYTotalUnits = 0 THEN 0
                  ELSE (((a.TotalUnits+a.TotalpromoUnits)-a.LYTotalUnits)/a.LYTotalUnits)*100
              END) PctYOYUnitsChange,
              (CASE
                  WHEN a.LYTotalTonnage = 0 THEN 0
                  ELSE ((a.TotalTonnage-a.LYTotalTonnage)/a.LYTotalTonnage)*100
              END) PctYOYTonnageChange,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalFEGP/(a.TotalSales+TotalpromoSales))*100
              END) PctMargin,
              (CASE
                  WHEN (a.TotalSales+a.TotalpromoSales) = 0 THEN 0
                  ELSE (a.TotalARCP/(a.TotalSales+TotalpromoSales))*100
              END) PctARCP,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (a.NumStrSelling/c.NumStrTotal)*100
              END) PctStrSelling,
              (CASE
                  WHEN c.NumStrTotal = 0 THEN 0
                  ELSE (b.NumStrPOGed/c.NumStrTotal)*100
              END) PctStrPOGed,
              a.NumStrSelling,
              a.FirstDtSold,
              a.NumWkSold,
              b.MinUnits,
              b.MaxUnits,
              b.MinFacings,
              b.MaxFacings,
              b.MinDaysSupply,
              b.MaxDaysSupply,
              b.Orientation,
              b.NumStrPOGed,
              a.LastDtSold,              
              b.avgfacings,
              b.avgdayssupply,
              b.srqmin,
              b.srqmax,
              b.srqavg,
              b.minretailfacings,
              b.maxretailfacings,
              b.avgretailfacings
         FROM src a
             left join prd_pog_str_rel b ON
               (
                  b.UPC = a.UPC
                  AND b.id = a.id
                  --s.f AND b.ng_code = a.ng_code
                  AND b.Division = a.Division
                  AND b.dbkey = a.dbkey
               )
         , assortment c
        WHERE
              c.DBParentProjectID = a.DBKey
          AND c.Division = a.Division
          AND c.Timebucket = a.Timebucket
--          AND c."Category" = a."Category"
--          AND c."Group"  = a."Group"
       )
       SELECT UPC,ID, --ng_code,
             Division,
              DBKey DBParentProjectID, TimeBucket,
              TotalSales, TotalUnits, TotalTonnage,
              TotalPromoSales, TotalPromoUnits, PctPromo,
              YOYSalesChange, YOYUnitsChange, YOYTonnageChange,
              PctYOYSalesChange, PctYOYUnitsChange, PctYOYTonnageChange,
              TotalFEGP, PctMargin, NumStrSelling, NumStrPOGed,
              PctStrSelling, PctStrPOGed,PctARCP, SPPD, AVGSPPD, UPPD,AvgUPPD,
              AvgPrice, AvgCost, FirstDtSold, Round(NumWkSold,0) NumWkSold, MinUnits, MaxUnits,
              MinFacings, MaxFacings, MinDaysSupply, MaxDaysSupply
             --s.f
             ,shrink
             ,Orientation,LastDtSold,              
              avgfacings,avgdayssupply,srqmin,srqmax,srqavg,
              minretailfacings,maxretailfacings,avgretailfacings
         FROM src2
;

-- Commit the changes
COMMIT WORK;

-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END aggregate_sales_division;

PROCEDURE ao_aggregate_sales
(
op_out_err         OUT NUMBER,
ip_ProjectKey     ix_ao_project.DBKEY%TYPE DEFAULT NULL
)
IS

-- Constants
LC_SUCCESS      CONSTANT NUMBER(1) := 0;
LC_ERROR        CONSTANT NUMBER(1) := 1;
--LC_EVENT_ID     CONSTANT PLS_INTEGER:= 20000;

-- Variables
--lv_ProjectID    ix_ao_project.DBKEY%TYPE;

v_err                               PLS_INTEGER;
v_nerrdbkey                         PLS_INTEGER;
v_ora_err_desc                      NVARCHAR2 (1000);

-- Added by S.S
v_c_sales_agg_start      CONSTANT   PLS_INTEGER := 2010005001;
v_c_sales_agg_finish     CONSTANT   PLS_INTEGER := 2010005002;
v_c_sales_agg_fail       CONSTANT   PLS_INTEGER := 2010005003;
--v_c_sales_agg_dntexst    CONSTANT   PLS_INTEGER := 2010005004;
v_c_sales_agg_empty      CONSTANT   PLS_INTEGER := 2010005005;

-- Cursors
CURSOR cur_projects IS
SELECT Name,
       DBKey,
       Desc1 Division,
       Desc2 Period,
       (SELECT COUNT(*) FROM ao_product_control WHERE DBParentProjectID = DBkey) product_control_count,
       (SELECT COUNT(*) FROM ao_assortment_control WHERE DBParentProjectID = DBKey) assortment_control_count
  FROM ix_ao_project
 WHERE dbkey = NVL(ip_Projectkey,dbkey)
 AND DBSTATUS IN (1,2,3)
 AND (Value3 = 1 OR Value5 = 1 )
 ORDER BY DBkey;

BEGIN

-- Log a message for starting
--ix_sys_event_log_insDB(LC_EVENT_ID, 'Procedure AO_Aggregate_Sales started');
-- Added by S.S
      DBMS_OUTPUT.put_line ('Procedure AO_Aggregate_Sales started ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));
      ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_sales_agg_start,
                            NULL,
                            NULL,
                            NULL);



DBMS_OUTPUT.put_line ('Step trunc ao_temp_project'  || ' ' || DBMS_UTILITY.get_time );
-- Purge the temp tables
AO_Purge_Package.AO_Truncate_Table('AO_TEMP_PROJECT');
FOR rec IN cur_projects LOOP
    IF 0 IN (rec.product_control_count,rec.assortment_control_count) THEN
        -- Log a message stating that the control tables has no data
        --ix_sys_event_log_insDB(LC_EVENT_ID,'One of the control tables has no data for the Project ' || ip_ProjectName);
        -- Added by S.S
        DBMS_OUTPUT.put_line ('One of the control tables has no data for the Project ' || ip_ProjectKey || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS') );
        ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_sales_agg_empty,
                            NULL,
                            NULL,
                            NULL,
                            'One of the control tables has no data for the Project ' || ip_ProjectKey);
    END IF;
   --DBMS_OUTPUT.put_line ('Processing Project ' || rec.ProjectName || ' ' || DBMS_UTILITY.get_time );
    -- Parse the period string and load the ao_temp_project table
    identify_project_time_bucket(rec.Dbkey, rec.Period);
END LOOP;

DBMS_OUTPUT.put_line ('Step aggregate_sales_store_group'  || ' ' || DBMS_UTILITY.get_time );
-- Aggregate sales at store group level
aggregate_sales_store_group(ip_ProjectKey);

DBMS_OUTPUT.put_line ('Step aggregate_sales_banner'  || ' ' || DBMS_UTILITY.get_time );
-- Aggregate sales at banner level
aggregate_sales_banner(ip_ProjectKey);
DBMS_OUTPUT.put_line ('Step aggregate_sales_region'  || ' ' || DBMS_UTILITY.get_time );
-- Aggregate sales at region level
aggregate_sales_region(ip_ProjectKey);
DBMS_OUTPUT.put_line ('Step aggregate_sales_division'  || ' ' || DBMS_UTILITY.get_time );
-- Aggregate sales at division level
aggregate_sales_division(ip_ProjectKey);

-- Log a message for completion
--ix_sys_event_log_insDB(LC_EVENT_ID, 'Procedure AO_Aggregate_Sales completed successfully');
-- Added by S.S



      DBMS_OUTPUT.put_line ('Procedure AO_Aggregate_Sales completed successfully ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS') );
      ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_sales_agg_finish,
                            NULL,
                            NULL,
                            NULL);

    op_out_err := LC_SUCCESS;
EXCEPTION
WHEN OTHERS THEN
      v_ora_err_desc := SQLERRM;
      DBMS_OUTPUT.put_line ('An Error occurred when executing procedure AO_Aggregate_Sales.');
      DBMS_OUTPUT.put_line (v_ora_err_desc || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));
      ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_sales_agg_fail,
                            NULL,
                            NULL,
                            NULL,
                            SUBSTR (v_ora_err_desc, 1, 1500));
      -- Return with an error code
      op_out_err := LC_ERROR;
      RAISE;

END ao_aggregate_sales;

PROCEDURE aggregate_sale_date
(
ip_wk_end_dt    ao_pos_data_sale_date.wk_end_dt%TYPE := NULL
)
IS

BEGIN

-- Insert data into the product table
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

IF ip_wk_end_dt IS NOT NULL
THEN
   -- Truncate/ Insert data only for the specified date
   AO_Purge_Package.AO_Truncate_Table(ip_wk_end_dt,'AO_POS_DATA_SALE_DATE');
ELSE
   -- Truncate/ Insert data for all the weeks
   AO_Purge_Package.AO_Truncate_Table('AO_POS_DATA_SALE_DATE');
END IF;

INSERT /*+ APPEND PARALLEL */
  INTO ao_pos_data_sale_date
       (scan_cd, artcl_num, str_site_num, wk_end_dt)
SELECT /*+ INDEX_FFS(A) PARALLEL_INDEX(A) */
       DISTINCT a.scan_cd, a.artcl_num, a.str_site_num, a.wk_end_dt
  FROM ao_pos_data a, ao_temp_TimeBucket b
 WHERE a.wk_end_dt = NVL(ip_wk_end_dt, a.wk_end_dt)
   AND b.year = 'C'
   AND a.wk_end_dt BETWEEN b.start_week AND b.end_week;

-- Commit the changes
COMMIT WORK;

EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END aggregate_sale_date;

-- Procedure to aggregate sales for a store
-- The procedure gets the last week loaded as a parameter
-- If none is specified it assumes the max week as the last week loaded
PROCEDURE aggregate_sales_store
(
ip_wk_end_dt    ao_pos_data.wk_end_dt%TYPE
)
IS

BEGIN

-- Enable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

EXECUTE IMMEDIATE 'CREATE TABLE ao_pos_data_store_new AS SELECT * FROM ao_pos_data_store WHERE 1 = 0';

EXECUTE IMMEDIATE '
INSERT /*+ APPEND PARALLEL */
  INTO ao_pos_data_store_new
       (
       TimeBucket, str_site_num, scan_cd, artcl_num, ng_code,
       FirstDtSold, sale_qty, sale_amt, tonnage, promo_qty, promo_amt, cost_amt,cost_amt_inv,
       shrink, arcp, ly_sale_qty, ly_sale_amt, ly_tonnage,LastDtSold
       )
WITH src AS
(
SELECT /*+ FULL(A) PARALLEL(A) */
       b.TimeBucket, a.str_site_num, a.scan_cd, a.artcl_num,
       MAX(a.ng_code) ng_code,
       MIN(DECODE(b.year,''C'',d.wk_end_dt,NULL)) FirstDtSold,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.sale_qty) sale_qty,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.sale_amt) sale_amt,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.tonnage) tonnage,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.promo_qty) promo_qty,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.promo_amt) promo_amt,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.cost_amt) cost_amt,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.cost_amt_inv) cost_amt_inv,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * c.shrink) shrink,
       SUM(DECODE(b.year,''C'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.ARCP) arcp,
       SUM(DECODE(b.year,''L'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.sale_qty) ly_sale_qty,
       SUM(DECODE(b.year,''L'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.sale_amt) ly_sale_amt,
       SUM(DECODE(b.year,''L'',1,0) * DECODE(a.wk_end_dt,b.start_week,-1,1) * a.tonnage) ly_tonnage,
       MAX(DECODE(b.year,''C'',d.wk_end_dt,NULL)) LastDtSold
  FROM ao_pos_data a
   left join ao_shrink_data c  ON (a.WK_END_DT = c.WK_END_DT
   AND a.ARTCL_NUM = c.ARTCL_NUM
   AND a.STR_SITE_NUM = c.STR_SITE_NUM)
  , ao_temp_timebucket b
  , ao_pos_data_sale_date d
 WHERE a.wk_end_dt IN (b.start_week, b.end_week)
   AND d.scan_cd = a.scan_cd
   AND d.artcl_num = a.artcl_num
   AND d.str_site_num = a.str_site_num
   AND d.wk_end_dt = a.wk_end_dt
 GROUP BY b.TimeBucket, a.str_site_num, a.scan_cd, a.artcl_num
)
SELECT a.TimeBucket, a.str_site_num, a.scan_cd, a.artcl_num, a.ng_code, a.firstdtsold,
       (a.sale_qty+b.sale_qty) sale_qty,
       (a.sale_amt+b.sale_amt) sale_amt,
       (a.tonnage+b.tonnage) tonnage,
       (a.promo_qty+b.promo_qty) promo_qty,
       (a.promo_amt+b.promo_amt) promo_amt,
       (a.cost_amt+b.cost_amt) cost_amt,
       (a.cost_amt_inv+b.cost_amt_inv) cost_amt_inv,
       (a.shrink+b.shrink) shrink,
       (a.arcp+b.arcp) arcp,
       (a.ly_sale_qty+b.ly_sale_qty) ly_sale_qty,
       (a.ly_sale_amt+b.ly_sale_amt) ly_sale_amt,
       (a.ly_tonnage+b.ly_tonnage) ly_tonnage,
       a.LastDtSold
  FROM src a, ao_pos_data_store b
 WHERE b.TimeBucket = a.TimeBucket
   AND b.str_site_num = a.str_site_num
   AND b.scan_cd = a.scan_cd
   AND b.artcl_num = a.artcl_num'
;

-- Commit the changes
COMMIT WORK;

-- Drop the old table
EXECUTE IMMEDIATE 'DROP TABLE ao_pos_data_store';
EXECUTE IMMEDIATE 'RENAME ao_pos_data_store_new TO ao_pos_data_store';

-- Disable Parallel DML
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END aggregate_sales_store;

-- Procedure to do the post processing after the data load
PROCEDURE ao_post_data_load
(
ip_fullordelta          CHAR := GC_DELTA,
ip_wk_end_dt            ao_pos_data.wk_end_dt%TYPE := NULL
)

IS

-- Variables
lv_wk_end_dt            ao_pos_data.wk_end_dt%TYPE;

v_c_post_load_start      CONSTANT   PLS_INTEGER := 2010008001;
v_c_post_load_finish     CONSTANT   PLS_INTEGER := 2010008002;
v_c_post_load_fail       CONSTANT   PLS_INTEGER := 2010008003;

v_err                               PLS_INTEGER;
v_nerrdbkey                         PLS_INTEGER;
v_ora_err_desc                      NVARCHAR2 (1000);

BEGIN

    DBMS_OUTPUT.put_line ('Procedure ao_post_data_load Started ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS') );
          ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_post_load_start,
                            NULL,
                            NULL,
                            NULL);

-- Use the max week if the week is not specified
IF ip_wk_end_dt IS NULL
THEN
  --S.F note. follow up do we want to use last week in ao_pos_data? or just current sysdate week from our known fiscal end day?
  --
    SELECT MAX(wk_end_dt)
      INTO lv_wk_end_dt
      FROM ao_pos_data a;
ELSE
    lv_wk_end_dt := ip_wk_end_dt;
END IF;

DBMS_OUTPUT.put_line ('Step Identify Time bucket. lv_wk_end_dt: ' || lv_wk_end_dt  || ' ' || DBMS_UTILITY.get_time );
-- Identify the time buckets
Identify_Data_Time_Bucket(lv_wk_end_dt);

IF ip_fullordelta = 'D'
THEN
    DBMS_OUTPUT.put_line ('Step Agg_sales_product with lv_wk_end_dt: ' || lv_wk_end_dt  || ' ' || DBMS_UTILITY.get_time );
   -- Maintain the product level summaries
   aggregate_sale_date(lv_wk_end_dt);
   DBMS_OUTPUT.put_line ('Step Agg_sales_store'  || ' ' || DBMS_UTILITY.get_time );
   -- Maintain the organization level summaries
   aggregate_sales_store(lv_wk_end_dt);
ELSE
    -- Run Replace Zero Cost procedure.
    DBMS_OUTPUT.put_line ('Procedure Replace Zero Cost'  || ' ' || DBMS_UTILITY.get_time );
    replace_zero_cost;
   -- Maintain the organization level summaries
      DBMS_OUTPUT.put_line ('Step B Agg_sales_store'  || ' ' || DBMS_UTILITY.get_time );
   aggregate_sales_store;
END IF;

  /*sys.dbms_stats.gather_table_stats(
      ownname          => 'AOSTG_PR',
      tabname          => 'ao_pos_data_store',
      estimate_percent => dbms_stats.auto_sample_size,
      cascade          => true,
      degree           => 8
   );*/

DBMS_OUTPUT.put_line ('Procedure ao_post_data_load completed successfully ' || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS') );
          ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_post_load_finish,
                            NULL,
                            NULL,
                            NULL);
EXCEPTION
WHEN OTHERS THEN

    -- Raise the error again

    -- Added by S.S
      v_ora_err_desc := SQLERRM;
      DBMS_OUTPUT.put_line ('An Error occurred when executing procedure ao_post_data_load.');
      DBMS_OUTPUT.put_line (SQLERRM || TO_CHAR(SYSDATE,'MM-DD-YYYY HH:MI:SS'));
      ix_sys_event_log_ins (v_err,
                            v_nerrdbkey,
                            v_c_post_load_fail,
                            NULL,
                            NULL,
                            NULL,
                            SUBSTR (v_ora_err_desc, 1, 1500));
      -- Return with an error code
    RAISE;

END ao_post_data_load;


PROCEDURE ao_stg_shrink_data_load
IS
BEGIN

EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';


   MERGE  /*+ append */ INTO   AO_SHRINK_DATA shr1
        USING   AO_SHRINK_STAGING shr2
           ON   (    shr1.ARTCL_NUM = shr2.artcl_num
                 AND shr1.str_site_num = shr2.str_site_num
                 AND shr1.wk_end_dt = shr2.wk_end_dt)
   WHEN NOT MATCHED
   THEN
      INSERT              (shr1.ARTCL_NUM,
                           shr1.STR_SITE_NUM,
                           shr1.WK_END_DT,
                           shr1.CORP_YR_NUM,
                           shr1.EXT_DT,
                           shr1.WK_NUM,
                           shr1.SHRINK)
          VALUES   (shr2.ARTCL_NUM,
                           shr2.STR_SITE_NUM,
                           shr2.WK_END_DT,
                           shr2.CORP_YR_NUM,
                           shr2.EXT_DT,
                           shr2.WK_NUM,
                           shr2.SHRINK)
   WHEN MATCHED
   THEN
      UPDATE SET
         shr1.SHRINK = shr2.SHRINK,
         shr1.CORP_YR_NUM = shr2.CORP_YR_NUM,
         shr1.EXT_DT = shr2.EXT_DT,
         shr1.WK_NUM = shr2.WK_NUM;

   COMMIT;

EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END ao_stg_shrink_data_load;

PROCEDURE ao_stg_pos_data_load
IS
BEGIN

EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = FALSE';
EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

-- Truncate the contents of the table
AO_Purge_Package.AO_Truncate_Table('AO_TEMP_POS_DATA');

INSERT INTO /*+ NO_STMT_QUEUING APPEND PARALLEL (12) */  ao_temp_pos_data
(wk_end_dt,artcl_num,str_site_num,scan_cd,sale_uom,ext_dt,wk_num,ng_code,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,base_uom)
WITH prod_dup as 
(
SELECT a.upc,a.id,a.dbkey,upc_ncd,a.ckb_sale_uom,
       ROW_NUMBER() OVER (PARTITION BY a.upc_ncd,a.id ORDER BY MAX(CASE WHEN pos.facings IS NULL THEN 0 ELSE pos.facings END) DESC,LENGTH(a.upc) DESC,a.ckb_sale_uom) rnk
       --Modified as part of September 2016 Suhas
       --ROW_NUMBER() OVER (PARTITION BY a.upc_ncd,a.id ORDER BY MAX(CASE WHEN pos.facings IS NULL THEN 0 ELSE pos.facings END) DESC,a.ckb_sale_uom) rnk1,
       --MAX(CASE WHEN pos.facings IS NULL THEN 0 ELSE pos.facings END) facings
  FROM (            
          SELECT upc_ncd,id,upc,dbkey,desc12 ckb_sale_uom
          FROM (
                SELECT DISTINCT CASE WHEN LENGTH (TRIM (upc)) IN (11, 12, 13, 14) THEN TO_CHAR(TO_NUMBER(SUBSTR ( TRIM(upc),1,(LENGTH(TRIM(upc))- 1)))) ELSE TRIM (upc) END as upc_ncd,
                       id,upc,dbkey,desc12,
                       COUNT(DISTINCT desc12) OVER (PARTITION BY id,CASE WHEN LENGTH (TRIM (upc)) IN (11, 12, 13, 14) THEN TO_CHAR(TO_NUMBER(SUBSTR ( TRIM(upc),1,(LENGTH(TRIM(upc))- 1)))) ELSE TRIM (upc) END) cnt
                  FROM ix_spc_product prod
                 WHERE prod.id in (select artcl_num from ao_pos_staging)
                   AND dbstatus = 1
                   AND desc19 = 'A1'
                   AND desc28 = 'A1'
               )
         WHERE cnt > 1             
       ) a
       LEFT JOIN
       ix_spc_performance pos
       ON (a.dbkey = pos.dbparentproductkey AND 
           pos.dbparentplanogramkey IN (SELECT dbkey FROM ix_spc_planogram WHERE dbstatus = 1)
          )
GROUP BY a.upc,a.id,a.dbkey,a.upc_ncd,a.ckb_sale_uom
),
prod AS 
(
SELECT upc_ncd upc,id,ckb_sale_uom
  FROM (
        SELECT DISTINCT CASE WHEN LENGTH (TRIM (upc)) IN (11, 12, 13, 14) THEN TO_CHAR(TO_NUMBER(SUBSTR ( TRIM(upc),1,(LENGTH(TRIM(upc))- 1)))) ELSE TRIM (upc) END as upc_ncd,
               id,desc12 ckb_sale_uom,
               COUNT(DISTINCT desc12) OVER (PARTITION BY id,CASE WHEN LENGTH (TRIM (upc)) IN (11, 12, 13, 14) THEN TO_CHAR(TO_NUMBER(SUBSTR ( TRIM(upc),1,(LENGTH(TRIM(upc))- 1)))) ELSE TRIM (upc) END) cnt
          FROM ix_spc_product prod
         WHERE prod.id IN (SELECT artcl_num FROM ao_pos_staging)
           AND dbstatus = 1
           AND desc19 = 'A1'
           AND desc28 = 'A1'
        )
 WHERE cnt = 1               
UNION
SELECT upc_ncd upc,id,ckb_sale_uom FROM prod_dup WHERE rnk = 1
/*
SELECT upc_ncd upc,id,ckb_sale_uom FROM prod_dup WHERE facings <> 0 and rnk1 = 1
UNION
SELECT upc_ncd upc,id,ckb_sale_uom FROM prod_dup WHERE facings = 0 and rnk = 1*/
),
upc_id_uom AS 
(
SELECT prod.ckb_sale_uom,pos.*
  FROM ao_pos_staging pos, prod
 WHERE pos.artcl_num = prod.id
   AND pos.scan_cd = prod.upc
),
upc_id_uom_diff AS
(
SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM ao_pos_staging
 MINUS
SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM upc_id_uom
),
id_uom_match AS 
(
SELECT MIN(ckb_sale_uom) OVER (PARTITION BY artcl_num) ckb_sale_uom,
       ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM (      
        SELECT DISTINCT prod.ckb_sale_uom,a.*
          FROM upc_id_uom_diff a,prod
         WHERE a.artcl_num = prod.id
           AND a.sale_uom = prod.ckb_sale_uom
       )
),
id_uom_match_diff AS
(
SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM upc_id_uom_diff
 MINUS
SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM id_uom_match
),
id_uom_unmatch AS
(
SELECT DISTINCT NVL(b.ckb_sale_uom,a.ckb_sale_uom) ckb_sale_uom,
       ext_dt,wk_num,wk_end_dt,a.artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom
  FROM (SELECT DISTINCT MIN(prod.ckb_sale_uom) OVER (PARTITION BY prod.id) ckb_sale_uom,a.*
          FROM id_uom_match_diff a,prod 
         WHERE prod.id = a.artcl_num) a
       LEFT JOIN
       (SELECT DISTINCT artcl_num,ckb_sale_uom FROM id_uom_match) b
    ON (a.artcl_num = b.artcl_num)             
),
upc_id_uom_unmatch AS
(
SELECT FIRST_VALUE(sale_uom) OVER (PARTITION BY scan_cd,artcl_num ORDER BY (sale_amt+promo_amt) DESC) ckb_sale_uom,pos.*
  FROM ao_pos_staging pos
 WHERE artcl_num NOT IN (SELECT id FROM prod) 
)
SELECT /*+ NO_STMT_QUEUING APPEND PARALLEL (12) */  wk_end_dt,artcl_num,str_site_num,scan_cd,ckb_sale_uom,
       MAX(ext_dt) ext_dt,
       MAX(wk_num) wk_num,
       MAX(ng_code) ng_code,
       SUM(promo_qty) promo_qty,
       SUM(promo_amt) promo_amt,
       SUM(sale_qty) sale_qty,
       SUM(sale_amt) sale_amt,
       AVG(cost_amt) cost_amt,
       MAX(vel_cd) vel_cd,
       AVG(cost_amt_inv) cost_amt_inv,
       MAX(fiscal_prd) fiscal_prd,
       SUM(arcp) arcp,
       SUM(tonnage) tonnage,
       SUM(promo_tonnage) promo_tonnage,
       MAX(base_uom) base_uom
FROM   (       
        SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom,ckb_sale_uom
          FROM upc_id_uom
        UNION
        SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom,ckb_sale_uom
          FROM id_uom_match
        UNION
        SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom,ckb_sale_uom
          FROM id_uom_unmatch
        UNION
        SELECT ext_dt,wk_num,wk_end_dt,artcl_num,str_site_num,ng_code,scan_cd,promo_qty,promo_amt,sale_qty,sale_amt,cost_amt,vel_cd,cost_amt_inv,fiscal_prd,arcp,tonnage,promo_tonnage,sale_uom,base_uom,ckb_sale_uom
          FROM upc_id_uom_unmatch
        )
GROUP BY wk_end_dt,artcl_num,str_site_num,scan_cd,ckb_sale_uom;

COMMIT;

   MERGE  /*+ append */ INTO   AO_POS_DATA pos1
        USING   AO_TEMP_POS_DATA pos2
           ON   (    pos1.SCAN_CD = pos2.scan_cd
                 AND pos1.ARTCL_NUM = pos2.artcl_num
                 AND pos1.str_site_num = pos2.str_site_num
                 AND pos1.wk_end_dt = pos2.wk_end_dt)
   WHEN NOT MATCHED
   THEN
      INSERT              (pos1.SCAN_CD,
                           pos1.ARTCL_NUM,
                           POS1.NG_CODE,
                           pos1.STR_SITE_NUM,
                           pos1.WK_END_DT,
                           pos1.PROMO_QTY,
                           pos1.PROMO_AMT,
                           pos1.SALE_QTY,
                           pos1.SALE_AMT,
                           pos1.COST_AMT,
                           pos1.COST_AMT_INV,
                           pos1.PROMO_TONNAGE,
                           pos1.TONNAGE,
                           pos1.ARCP,
                           pos1.VEL_CD,
                           pos1.FISCAL_PRD,
                           pos1.EXT_DT,
                           pos1.WK_NUM,
                           pos1.SALE_UOM,
                           pos1.BASE_UOM)
          VALUES   (POS2.SCAN_CD,
                    pos2.ARTCL_NUM,
                    pos2.NG_CODE,
                    pos2.STR_SITE_NUM,
                    pos2.WK_END_DT,
                    pos2.PROMO_QTY,
                    pos2.PROMO_AMT,
                    pos2.SALE_QTY,
                    pos2.SALE_AMT,
                    pos2.COST_AMT,
                    pos2.COST_AMT_INV,
                    pos2.PROMO_TONNAGE,
                    pos2.TONNAGE,
                    pos2.ARCP,
                    pos2.VEL_CD,
                    pos2.FISCAL_PRD,
                    pos2.EXT_DT,
                    pos2.WK_NUM,
                    pos2.SALE_UOM,
                    pos2.BASE_UOM)
   WHEN MATCHED
   THEN
      UPDATE SET
         pos1.NG_CODE        =pos2.NG_CODE,
         pos1.PROMO_QTY = pos1.PROMO_QTY + pos2.PROMO_QTY,
         pos1.PROMO_AMT = pos1.PROMO_AMT + pos2.PROMO_AMT,
         pos1.SALE_QTY = pos1.SALE_QTY + pos2.SALE_QTY,
         pos1.SALE_AMT = pos1.SALE_AMT + pos2.SALE_AMT,
--         pos1.COST_AMT = pos2.COST_AMT,
--         pos1.COST_AMT_INV = pos2.COST_AMT_INV,
         pos1.PROMO_TONNAGE = pos1.PROMO_TONNAGE + pos2.PROMO_TONNAGE,
         pos1.TONNAGE = pos1.TONNAGE + pos2.TONNAGE,
         pos1.ARCP = pos1.ARCP + pos2.ARCP,
         pos1.VEL_CD = pos2.VEL_CD,
         pos1.FISCAL_PRD = pos2.FISCAL_PRD,
         pos1.EXT_DT = pos2.EXT_DT,
         pos1.WK_NUM = pos2.WK_NUM,
         pos1.SALE_UOM = pos2.SALE_UOM,
         pos1.BASE_UOM = pos2.BASE_UOM;

   COMMIT;
   
EXECUTE IMMEDIATE 'ALTER SESSION SET query_rewrite_enabled = TRUE';
EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

END ao_stg_pos_data_load;

END ao_aggregate_package;
/

